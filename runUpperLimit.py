
def runUpperLimits():
    from ROOT import Util, RooExpandedFitResult, RooArgSet, RooAddition

    if len(sigReg)>1:
        print 'ERROR: can only process SRs one by one'
        return
    region=sigReg[0]+'_disc_cuts'

    fitStatus='beforeFit' # don't use 'afterFit'

    ## The following is largely inspired (to not say copy-pasted) from HistFitter/scripts/YieldTables.py

    filename=pathUtilities.histFitterTopDirectory()+'/results/Discovery__summer2013SameSign/bkgOnly_combined_NormalMeasurement_model_afterFit.root'
    w = Util.GetWorkspaceFromFile(filename,'w')
    w.loadSnapshot('snapshot_paramsVals_RooExpandedFitResult_%s'%(fitStatus))

    pdfinRegion = Util.GetRegionPdf(w, region)
    varinRegion =  Util.GetRegionVar(w, region)

    prodList = pdfinRegion.pdfList()
    for idx in range(prodList.getSize()):
        if prodList[idx].InheritsFrom('RooRealSumPdf'):
            rrspdfinRegion =  prodList[idx].createIntegral(RooArgSet(varinRegion))
    nsig = Util.GetComponent(w,'DiscoveryMode_'+sigReg[0],region,False).getVal()
    nbkg = rrspdfinRegion.getVal() - nsig
    fitresult = w.obj('RooExpandedFitResult_%s'%(fitStatus))
    err_nbkg = Util.GetPropagatedError(rrspdfinRegion, fitresult)

    data_set = w.data('obsData')
    regionCat = w.obj('channelCat')
    regionFullName = Util.GetFullRegionName(regionCat,region)
    regionDataset = data_set.reduce('channelCat==channelCat::' +regionFullName.Data())
    ndata = regionDataset.sumEntries()

    print 'Observed data:',ndata
    print 'Expected background:',nbkg,'+/-',err_nbkg

    # Now create simple fit configuration

    lumiError = 0.036 	# Relative luminosity uncertainty
    configMgr.fixSigXSec=False

    configMgr.nTOYs=3000
    configMgr.calculatorType=2 # 2=asymptotic calculator, 0=frequentist calculator
    configMgr.testStatType=3   # 3=one-sided profile likelihood test statistic (LHC default)
    configMgr.nPoints=20       # number of values scanned of signal-strength for upper-limit determination of signal strength.
    configMgr.analysisName = "UpperLimits_%s__summer2013SameSign"%sigReg[0]
    configMgr.outputFileName = "results/%s_output.root"%configMgr.analysisName
    configMgr.cutsDict[sigReg[0]] = "1."
    configMgr.weights = "1."

    poi_name="mu_"+sigReg[0]

    all_err=err_nbkg/nbkg
    ucb_bkg = Systematic("ucb_bkg", configMgr.weights, 1.+all_err,1.-all_err, "user","userOverallSys")

    import configWriter
    Bkg = configWriter.Sample("Bkg",0)
    Bkg.setStatConfig(False)
    Bkg.setNormByTheory(False) 
    Bkg.buildHisto([nbkg],sigReg[0],"cuts")
    Bkg.addSystematic(ucb_bkg)

    nsig = 1
    sigSample = configWriter.Sample("Sig",0)
    sigSample.setNormFactor(poi_name,1.,0.,10.)
    sigSample.setStatConfig(False)
    sigSample.setNormByTheory(False) 
    sigSample.buildHisto([nsig],sigReg[0],"cuts")

    dataSample = configWriter.Sample("Data",0)
    dataSample.setData()
    dataSample.buildHisto([ndata],sigReg[0],"cuts")

    exclusionFitConfig = configMgr.addFitConfig("SplusB")
    exclusionFitConfig.addSamples([Bkg,sigSample,dataSample])  
    #exclusionFitConfig.setSignalSample(sigSample)

    meas = exclusionFitConfig.addMeasurement(name="NormalMeasurement",lumi=1.0,lumiErr=lumiError)
    meas.addPOI(poi_name)    
    meas.addParamSetting("Lumi","const",1.0)

    chan = exclusionFitConfig.addChannel("cuts",[sigReg[0]],1,0.,1.)
    exclusionFitConfig.setSignalChannels([chan])  

    if configMgr.executeHistFactory:
        if os.path.isfile("data/%s.root"%configMgr.analysisName):
            os.remove("data/%s.root"%configMgr.analysisName) 
    return
