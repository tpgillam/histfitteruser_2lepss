#!/usr/bin/env python


# run as:
# HistFitter.py -twf -u "--signalRegions SR0bee,SR0bem,SR0bmm,SR1bee,SR1bem,SR1bmm,SR3bee,SR3bem,SR3bmm --signalGridName Mc12SusyGtt --signalList signal156548 --fitMode SRonly" python/summer2013SameSign.py

from configManager import configMgr
from systematic import Systematic
from optparse import OptionParser
    
import  ROOT, math, sys, os

# CHANGE NTOYS, appended path, signal File!!!!!!
sys.path.append('python') #this is a hack to import the sameSignTools from /python
import pathUtilities
import signalSameSignTools
signalRunnumberToMassesDict = signalSameSignTools.getRunnumbersMassesDictSSAllGrids()


 # global variables, input flags with defaults
bkgOnlyFitConfig = None

inputParser = OptionParser()
inputParser.add_option('', '--calculatorType', dest = 'calculatorType', default = 2, help='0 for frequentist, 2 for asymptotic')
inputParser.add_option('', '--signalGridName', dest = 'signalGridName', default = 'Mc12SusyGtt', help='name of the grid(e.g. gtt)')
inputParser.add_option('', '--signalList', dest = 'signalList', default = 'signal156548', help='give it a list of signal runnumbers')
inputParser.add_option('', '--useStat', dest = 'useStat', default = "MergedSamples", help='MergedSamples, PerSample, None')
inputParser.add_option('', '--fitMode', dest = 'fitMode', default = "SR", help='pre-defined fit configurations: SR_discovery')
inputParser.add_option('', '--theoryUncertMode', dest = 'theoryUncertMode', default = "nom", help='sets theory (i.e. lumi uncert) nom/up/down')
inputParser.add_option('', '--additionalOutName', dest = 'additionalOutName', default = "", help='additional name for output file')
inputParser.add_option('', '--signalRegions', dest = 'signalRegions', default = 'SR0b,SR1b,SR3b,SR3Llow,SR3Lhigh', help='sets which signal regions to use,(one or several), 0b,1b and/or 3b')
inputParser.add_option('', '--controlRegions', dest = 'controlRegions', default = '', help='sets which control regions to use,(one or several)')
inputParser.add_option('', '--validationRegions', dest = 'validationRegions', default = "", help='sets which validation regions to use,(one or several)')
inputParser.add_option('', '--useComplexFakeUncertainty', dest = 'useComplexFakeUncertainty', default = 'False', help='whether to use the simplified (bit worse) or fully correlated fake uncertainties (facotr ~2 slower)')
inputParser.add_option('', '--useCoarseGranularity', dest = 'useCoarseGranularity', default = "False", help='do we merge all the diboson and all the top+X backgroudns together?')

import select
configArgs = []

userArgs= configMgr.userArg.split(' ')
for i in userArgs:
    configArgs.append(i)

(options, args) = inputParser.parse_args(configArgs)
fitMode = options.fitMode.split("_")
signalRegionList = options.signalRegions.split(",")
controlRegionList = options.controlRegions.split(",")
validationRegionList = options.validationRegions.split(",")
splitTopV = True

if options.useComplexFakeUncertainty == 'True': useComplexFakeUncertainty = True
else: useComplexFakeUncertainty = False

if options.useCoarseGranularity == 'True': useCoarseGranularity = True
else: useCoarseGranularity = False

discoveryFit = (("discovery" in fitMode) or ("disc" in fitMode))
if discoveryFit:
    options.signalGridName="Discovery"

def runAll():

    #Note: skimmed Trees require (>=3 jets pT>40) OR (>=1 bjet pT>20)
    skimOrFullSuffix = "_skimmed"

    for vr in validationRegionList:
        if vr.startswith("VRVV"):
            skimOrFullSuffix = ""
    for controlRegion in controlRegionList:
        if 'CRVV' in controlRegion:  skimOrFullSuffix = ""
       

    ttreeDirectory = pathUtilities.currentTreeDirectory()

    inputFileDict = {}
    inputFileDict['data'] = [ttreeDirectory + '/egamma.root', ttreeDirectory + '/muon.root',  ttreeDirectory + '/jettauetmiss.root']
    inputFileDict['signal'] = [ttreeDirectory + '/signal.root']
    inputFileDict['background'] = [ttreeDirectory + '/backgrounds.root']

    inputFileDict['fakes'] = [ttreeDirectory + "/fakeEstimate" +skimOrFullSuffix+ ".root" ]
    inputFileDict['misId'] = [ttreeDirectory + "/chargeFlip" +skimOrFullSuffix + ".root"]
    

    setupConfigMgr(inputFileDict)

    if options.signalList=="bkgOnly":
        doFitConfig(inputFileDict,"bkgOnly")
    else:
        signalGridName = options.signalGridName
        signalNamesList = options.signalList.split(';')
        for signalName in signalNamesList:
            doFitConfig(inputFileDict,signalGridName+"_"+signalName, signalName)


def setupConfigMgr(inputFileDict):
    #-------------------------------
    # Parameters for hypothesis test
    #-------------------------------
    configMgr.analysisName = options.signalGridName+'_'+options.additionalOutName+'_summer2013SameSign'
    
    configMgr.nTOYs= 1000
    configMgr.calculatorType = int(options.calculatorType)
    configMgr.testStatType = 3   # 3=one-sided profile likelihood test statistic (LHC default)
    configMgr.nPoints = 20       # number of values scanned of signal-strength for upper-limit determination of signal strength.
    
    configMgr.blindSR = False
    configMgr.blindCR = False
    configMgr.blindVR = False
    configMgr.writeXML = False #True is for debugging
    
    configMgr.outputLumi = 20.3
    configMgr.inputLumi = configMgr.outputLumi #deal with this at the sample level
    configMgr.setLumiUnits("fb-1")

    configMgr.histCacheFile = "data/"+configMgr.analysisName+".root"
    configMgr.outputFileName = "results/"+configMgr.analysisName+"_output.root"
    
    if not configMgr.readFromTree:
        inputFileDict['background'] = ["data/"+configMgr.analysisName+".root"]
        
    configMgr.nomName = "_nom"

    ##########
    ## CUTS ##
    ##########
    
    # n.b. "number of leptons above 15 GeV. Trees only contain events that pass our selection which involves at least one 20 GeV lepton per event"
    # thus, this means that effectively the variable means nLep15 > x and nLep20 > 0
    configMgr.cutsDict["SR3b"] = "nLep15>=2 && nJets40>= 5 && nBJets20>=3"
    configMgr.cutsDict["SR3b_disc"] = configMgr.cutsDict["SR3b"] + "&& meff > 350000"
    configMgr.cutsDict["SR3b_bin1"] = configMgr.cutsDict["SR3b"]+" && meff>190000 && meff<890000"
    configMgr.cutsDict["SR3b_bin2"] = configMgr.cutsDict["SR3b"]+" && meff>890000"
    
    configMgr.cutsDict["SR0b"] = "nLep15 ==2 & met>150000 && nJets40>=3 && nBJets20==0 && mt>100000"
    configMgr.cutsDict["SR0b_disc"] = configMgr.cutsDict["SR0b"]+" && meff>400000"
    configMgr.cutsDict["SR0b_bin1"] = configMgr.cutsDict["SR0b"]+" && meff>300000 && meff<600000"
    configMgr.cutsDict["SR0b_bin2"] = configMgr.cutsDict["SR0b"]+" && meff>600000 && meff<900000"
    configMgr.cutsDict["SR0b_bin3"] = configMgr.cutsDict["SR0b"]+" && meff>900000 && meff<1200000"
    configMgr.cutsDict["SR0b_bin4"] = configMgr.cutsDict["SR0b"]+" && meff>1200000"

    configMgr.cutsDict["SR1b"] = "nLep15==2 && met>150000 && nJets40>=3 && nBJets20>=1 && mt>100000 && !("+configMgr.cutsDict["SR3b"]+")"
    configMgr.cutsDict["SR1b_disc"] = configMgr.cutsDict["SR1b"]+" && meff>700000"
    configMgr.cutsDict["SR1b_bin1"] = configMgr.cutsDict["SR1b"]+" && meff>300000 && meff<700000"
    configMgr.cutsDict["SR1b_bin2"] = configMgr.cutsDict["SR1b"]+" && meff>700000 && meff<1100000"
    configMgr.cutsDict["SR1b_bin3"] = configMgr.cutsDict["SR1b"]+" && meff>1100000"

    configMgr.cutsDict["SR3Llow"] = "nLep15>=3 && nJets40>=4 && met > 50000 && met<150000 && (mllSFOS1 < 84000 || mllSFOS1 > 98000) &&  (mllSFOS2 < 84000 || mllSFOS2 > 98000)  && !("+configMgr.cutsDict["SR3b"]+")"
    configMgr.cutsDict["SR3Llow_disc"] = configMgr.cutsDict["SR3Llow"] + " && meff>400000"
    configMgr.cutsDict["SR3Llow_bin1"] = configMgr.cutsDict["SR3Llow"]+" && meff>255000 && meff<727500"
    configMgr.cutsDict["SR3Llow_bin2"] = configMgr.cutsDict["SR3Llow"]+" && meff>727500"

    configMgr.cutsDict["SR3Lhigh"] = "nLep15>=3 && nJets40>=4 && met>150000 && !("+configMgr.cutsDict["SR3b"]+")"
    configMgr.cutsDict["SR3Lhigh_disc"] = configMgr.cutsDict["SR3Lhigh"] + " && meff>400000"
    configMgr.cutsDict["SR3Lhigh_bin1"] = configMgr.cutsDict["SR3Lhigh"]+" && meff>355000 && meff<1077500"
    configMgr.cutsDict["SR3Lhigh_bin2"] = configMgr.cutsDict["SR3Lhigh"]+" && meff>1077500"

    configMgr.cutsDict["SRall"] = configMgr.cutsDict["SR0b"]+"||"+configMgr.cutsDict["SR1b"]+"||"+configMgr.cutsDict["SR3b"]+"||"+configMgr.cutsDict["SR3Llow"]+"||"+configMgr.cutsDict["SR3Lhigh"]
    configMgr.cutsDict["SRall_disc"] = configMgr.cutsDict["SR0b_disc"]+"||"+configMgr.cutsDict["SR1b_disc"]+"||"+configMgr.cutsDict["SR3b_disc"]+"||"+configMgr.cutsDict["SR3Llow_disc"]+"||"+configMgr.cutsDict["SR3Lhigh_disc"]
    configMgr.cutsDict["SRfive_disc"] = configMgr.cutsDict["SRall_disc"]
    configMgr.cutsDict["SRSS_disc"] = configMgr.cutsDict["SR0b_disc"]+"||"+configMgr.cutsDict["SR1b_disc"]
    configMgr.cutsDict["SRLLL_disc"] = configMgr.cutsDict["SR3Llow_disc"]+"||"+configMgr.cutsDict["SR3Lhigh_disc"]

    configMgr.cutsDict["CRttZ"] = "nLep20>=3 && lep3Pt>10000 && nBJets20==1 && nJets40>=2 && met>20000 && met<120000 && meff>=300000 && ((mllSFOS2>84000&&mllSFOS2<98000)||(mllSFOS1>84000&&mllSFOS1<98000))"
    configMgr.cutsDict["CRVV"] = "nLep20>=2 && nBJets20==0 && nJets20>=2 && met>20000 && met<120000 && chanDilep==2 && mt>=100000"

    
    configMgr.cutsDict["VRttW"] = "nLep20==2 && nBJets20==2&& nJets30>=1 && met>20000 && met<120000 && mt>=80000 && chanDilep == 2"
    configMgr.cutsDict["VRttZ"] = configMgr.cutsDict["CRttZ"]
    configMgr.cutsDict["VRVV"] = configMgr.cutsDict["CRVV"]
    
    for region in ["SR0b","SR1b","SR3b", "SR3Llow", "SR3Lhigh", "VRttZ"]:
        configMgr.cutsDict[region+"ee"] = configMgr.cutsDict[region] + " && chanDilep == 0"
        configMgr.cutsDict[region+"em"] = configMgr.cutsDict[region] + " && chanDilep == 1"
        configMgr.cutsDict[region+"mm"] = configMgr.cutsDict[region] + " && chanDilep == 2"
        if region.startswith('SR'):
            configMgr.cutsDict[region+"ee_disc"] = configMgr.cutsDict[region+"_disc"] + " && chanDilep==0"
            configMgr.cutsDict[region+"em_disc"] = configMgr.cutsDict[region+"_disc"] + " && chanDilep==1"
            configMgr.cutsDict[region+"mm_disc"] = configMgr.cutsDict[region+"_disc"] + " && chanDilep==2"
        pass
    
    return

def doFitConfig(inputFileDict, fcName, sigSample=""):
    #---------
    # weights 
    #---------
    if options.theoryUncertMode.lower() == 'up':
        configMgr.weights = ("mcWgt", "pileupWgt", "trigWgt", "eGammaWgt", "muonWgt", "lumiScaling_UP" ,"bTagWgt")
    elif options.theoryUncertMode.lower() == 'down':
        configMgr.weights = ("mcWgt", "pileupWgt", "trigWgt", "eGammaWgt", "muonWgt", "lumiScaling_DOWN" ,"bTagWgt")
    else:
        configMgr.weights = ("mcWgt", "pileupWgt", "trigWgt", "eGammaWgt", "muonWgt", "lumiScaling" ,"bTagWgt")
    
    muonUpWeights = replaceStringInTuple(configMgr.weights, "muonWgt", "muonWgt_UP")
    muonDownWeights = replaceStringInTuple(configMgr.weights, "muonWgt", "muonWgt_DOWN")
    
    egammaUpWeights = replaceStringInTuple(configMgr.weights, "eGammaWgt", "eGammaWgt_UP")
    egammaDownWeights = replaceStringInTuple(configMgr.weights, "eGammaWgt", "eGammaWgt_DOWN")

    pileupUpWeights = replaceStringInTuple(configMgr.weights, "pileupWgt", "pileupWgt_UP")
    pileupDownWeights = replaceStringInTuple(configMgr.weights, "pileupWgt", "pileupWgt_DOWN")

    btagUpWeights = replaceStringInTuple(configMgr.weights + ("bTagWgt_MISTAGUP", ), "bTagWgt", "bTagWgt_BJETUP")
    btagDownWeights = replaceStringInTuple(configMgr.weights + ("bTagWgt_MISTAGDOWN", ),"bTagWgt", "bTagWgt_BJETDOWN")

    triggerWeights = replaceStringInTuple(configMgr.weights, "trigWgt", "trigWgt_UP")

    pdfUpWeights = configMgr.weights + ("pdfWgt_UP", )
    pdfDownWeights = configMgr.weights + ("pdfWgt_DOWN", )
    
    #---------------#
    # Build samples #
    #---------------#
    import configWriter
    backgroundDict = {}

    if len(signalRegionList)==1:
        my_mu_SIG = "mu_"+signalRegionList[0]
    else:
        my_mu_SIG = "mu_SIG"

    if len(sigSample)>0:
        signalName = str(sigSample)
        backgroundDict[signalName] = configWriter.Sample(signalName, ROOT.kPink)
        backgroundDict[signalName].setFileList(inputFileDict['signal'])
        backgroundDict[signalName].setNormByTheory() # assigns lumi uncertainty
        backgroundDict[signalName].setNormFactor(my_mu_SIG, 1., 0., 3.)           


    if useCoarseGranularity:
        backgroundDict['diBoson'] = configWriter.Sample("diBoson", ROOT.kAzure - 1)
        backgroundDict['diBoson'].setFileList(inputFileDict['background'])
        diBosonWVSR3b  = configWriter.Sample("diBoson", 21)
        diBosonWVSR3b.setFileList(inputFileDict['background'])
        
        backgroundDict['topX'] = configWriterSample("topX", ROOT.kSpring-5)
        backgroundDict['topX'].setFileList(inputFileDict['background'])
    else:

        # 'diBoson': ['diBosonPowhegZZ', 'diBosonWW', 'diBosonWZ', 'triBoson'],

        backgroundDict['diBosonWZ'] = configWriter.Sample("diBosonWZ", ROOT.kAzure-1)
        backgroundDict['diBosonWZ'].setFileList(inputFileDict['background'])
        backgroundDict['triBoson'] = configWriter.Sample("triBoson", ROOT.kAzure+1)
        backgroundDict['triBoson'].setFileList(inputFileDict['background'])
        backgroundDict['diBosonZZ'] = configWriter.Sample("diBosonPowhegZZ", ROOT.kAzure+3)
        backgroundDict['diBosonZZ'].setFileList(inputFileDict['background'])
        backgroundDict['diBosonSS'] = configWriter.Sample("diBosonSS", ROOT.kAzure-5)
        backgroundDict['diBosonSS'].setFileList(inputFileDict['background'])
           
        #backgroundDict['diBosonVVJJEW6'] = configWriter.Sample("diBosonVVJJEW6", ROOT.kRed-5)
        #backgroundDict['diBosonVVJJEW6'].setFileList(inputFileDict['background'])

        backgroundDict['topW'] = configWriter.Sample("topW", ROOT.kSpring-5)
        backgroundDict['topW'].setFileList(inputFileDict['background'])
        backgroundDict['topZ'] = configWriter.Sample("topZ", ROOT.kSpring+5)
        backgroundDict['topZ'].setFileList(inputFileDict['background'])
    
        backgroundDict['ttbarHiggs'] = configWriter.Sample("ttbarHiggs", ROOT.kSpring+8)
        backgroundDict['ttbarHiggs'].setFileList(inputFileDict['background'])

        backgroundDict['singleTopZ'] = configWriter.Sample("singleTopZ", ROOT.kSpring+1)
        backgroundDict['singleTopZ'].setFileList(inputFileDict['background'])
    
        backgroundDict['fourTop'] = configWriter.Sample("fourTop", ROOT.kSpring+1)
        backgroundDict['fourTop'].setFileList(inputFileDict['background'])

    if options.useStat=="MergedSamples":
        for background in backgroundDict:
            backgroundDict[background].setStatConfig(True)
    elif options.useStat=="PerSample":
        
        if useCoarseGranularity:
            backgroundDict['diBoson'].addSystematic(Systematic("mcstat_diBoson", "_nom", "_nom", "_nom", "tree", "shapeStat"))
            diBosonWVSR3b.addSystematic(Systematic("mcstat_diBoson", "_nom", "_nom", "_nom", "tree", "shapeStat"))
            backgroundDict['topX'].addSystematic(Systematic("mcstat_ttX", "_nom", "_nom", "_nom", "tree", "shapeStat"))
        else:
             backgroundDict['diBosonWZ'].addSystematic(Systematic("mcstat_diBosonWZ", "_nom", "_nom", "_nom", "tree", "shapeStat"))
             backgroundDict['triBoson'].addSystematic(Systematic("mcstat_triBoson", "_nom", "_nom", "_nom", "tree", "shapeStat"))
             backgroundDict['diBosonZZ'].addSystematic(Systematic("mcstat_diBosonZZ", "_nom", "_nom", "_nom", "tree", "shapeStat"))
             backgroundDict['diBosonSS'].addSystematic(Systematic("mcstat_diBosonSS", "_nom", "_nom", "_nom", "tree", "shapeStat"))

             backgroundDict['topW'].addSystematic(Systematic("mcstat_ttV", "_nom", "_nom", "_nom", "tree", "shapeStat"))
             backgroundDict['topZ'].addSystematic(Systematic("mcstat_ttV", "_nom", "_nom", "_nom", "tree", "shapeStat"))
             backgroundDict['singleTopZ'].addSystematic(Systematic("mcstat_singleTopZ", "_nom", "_nom", "_nom", "tree", "shapeStat"))
             backgroundDict['ttbarHiggs'].addSystematic(Systematic("mcstat_ttbarHiggs", "_nom", "_nom", "_nom", "tree", "shapeStat"))

        if len(sigSample)>0 and not discoveryFit:
            backgroundDict[signalName].addSystematic(Systematic("mcstat_sig", "_nom", "_nom", "_nom", "tree", "shapeStat"))

    if len(sigSample)>0  and discoveryFit:

        backgroundDict[signalName].setStatConfig(False)

    ###########################################
    #x-sec errors from https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/TTplusV
    
    if useCoarseGranularity:
        backgroundDict['diBoson'].addSystematic(Systematic("theoryUncertVV", configMgr.weights, 1.35, 0.65, "user", "userOverallSys"))
        # are neglecting other systematics, since backgroudn is tiny and this uncertainy trumps all the others.
        diBosonWVSR3b.addSystematic(Systematic("theoryUncert", configMgr.weights, 2.68, 0., "user", "userOverallSys"))
        
        backgroundDict['topX'].addSystematic(Systematic("xSecTTX", configMgr.weights, 1.3, 0.7, "user", "userOverallSys"))
        #backgroundDict['topX'].addSystematic(Systematic("ttVAlpgen", "_nom", "Alpgen_nom", "_nom", "tree", "histoSys"))
        #backgroundDict['topX'].addSystematic(Systematic("ttVjetMatching", "_nom", "AlternateJet_nom", "_nom", "tree", "histoSys"))
    else:
        backgroundDict['diBosonSS'].addSystematic(Systematic("theoryUncertWV", configMgr.weights, 1.35, 0.65, "user", "userOverallSys"))
        backgroundDict['diBosonWZ'].addSystematic(Systematic("theoryUncertWV", configMgr.weights, 1.35, 0.65, "user", "userOverallSys"))
        backgroundDict['triBoson'].addSystematic(Systematic("theoryUncertWV", configMgr.weights, 1.35, 0.65, "user", "userOverallSys"))
        backgroundDict['diBosonZZ'].addSystematic(Systematic("theoryUncertZZ", configMgr.weights, 1.8, 0.2, "user", "userOverallSys"))
                
        # this correlates the Alpgen and JetMatching systematics across the two samples   
        backgroundDict['topW'].addSystematic(Systematic("xSecTTW", configMgr.weights, 1.3, 0.7, "user", "userOverallSys"))
        backgroundDict['topW'].addSystematic(Systematic("ttVAlpgen", "_nom", "Alpgen_nom", "_nom", "tree", "histoSysOneSideSym"))
        backgroundDict['topW'].addSystematic(Systematic("ttVjetMatching", "_nom", "AlternateJet_nom", "_nom", "tree", "histoSysOneSideSym"))
        backgroundDict['topZ'].addSystematic(Systematic("xSecTTZ", configMgr.weights, 1.3, 0.7, "user", "userOverallSys"))
        backgroundDict['topZ'].addSystematic(Systematic("ttVAlpgen", "_nom", "Alpgen_nom", "_nom", "tree", "histoSysOneSideSym"))
        backgroundDict['topZ'].addSystematic(Systematic("ttVjetMatching", "_nom", "AlternateJet_nom", "_nom", "tree", "histoSysOneSideSym"))
    
        backgroundDict['singleTopZ'].addSystematic(Systematic("theoryUncertSingleTopZ", configMgr.weights, 1.5, 0.5, "user", "userOverallSys"))
        backgroundDict['fourTop'].addSystematic(Systematic("theoryUncertFourTops", configMgr.weights, 2.0, 0.0, "user", "userOverallSys"))

        backgroundDict['ttbarHiggs'].addSystematic(Systematic("theoryUncertTTbarHiggs", configMgr.weights, 2.0, 0.0, "user", "userOverallSys"))
    #from https://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/CONFNOTES/ATLAS-CONF-2012-154/
    #backgroundDict['diBosonWV'].addSystematic(Systematic("xSecDiBoson", configMgr.weights, 1.07, 0.93, "user", "userOverallSys"))
    #backgroundDict['diBosonZZ'].addSystematic(Systematic("xSecDiBoson", configMgr.weights, 1.07, 0.93, "user", "userOverallSys"))
    #backgroundDict['diBosonZZ'].addSystematic(Systematic("SherpaZZ", "_nom", "diBosonSherpaZZ_nom", "_nom", "tree", "normHistoSys"))
    

    for background in backgroundDict:
        histoSysType = "histoSys"
        overallSysType = "overallSys"
        
        backgroundDict[background].setNormByTheory(True) # assigns lumi uncertainty
                 
        backgroundDict[background].addSystematic(Systematic("JES", "_nom", "_JESUP", "_JESDOWN", "tree", histoSysType))
        backgroundDict[background].addSystematic(Systematic("JER", "_nom", "_JER", "_nom", "tree", histoSysType))
        backgroundDict[background].addSystematic(Systematic("SCALEST", "_nom", "_SCALESTUP", "_SCALESTDOWN", "tree", histoSysType))
        backgroundDict[background].addSystematic(Systematic("RESOST", "_nom", "_RESOST", "_nom", "tree", histoSysType))

        backgroundDict[background].addSystematic(Systematic("muReco", configMgr.weights, muonUpWeights, muonDownWeights, "weight", overallSysType))
        backgroundDict[background].addSystematic(Systematic("elReco", configMgr.weights, egammaUpWeights, egammaDownWeights, "weight", overallSysType))
        backgroundDict[background].addSystematic(Systematic("pileup", configMgr.weights, pileupUpWeights, pileupDownWeights, "weight", overallSysType))
        backgroundDict[background].addSystematic(Systematic("bTag", configMgr.weights, btagUpWeights, btagDownWeights, "weight", overallSysType))

        backgroundDict[background].addSystematic(Systematic("triggerWgt", configMgr.weights, triggerWeights, configMgr.weights, "weight", overallSysType))

        backgroundDict[background].addSystematic(Systematic("pdfWgt", configMgr.weights, pdfUpWeights, pdfDownWeights, "weight", overallSysType))

 
    ## Fakes ##
    # Note to self: should still be cleaned up, currently duplicating everything to add 50% uncertainty on SR3b. DC & TJM, 29/5
        
    weightsFakes= ("fakeLeptWgt", "chargeFlipWgt")
    weightsFakesElUp= ("fakeLeptWgt_El_UP", "chargeFlipWgt")
    weightsFakesMuUp= ("fakeLeptWgt_Mu_UP", "chargeFlipWgt")
    weightsFakesFlipUp= ("fakeLeptWgt", "chargeFlipWgt_UP")

    weightsFakes3b= ("fakeLeptWgt", "chargeFlipWgt", "1.0")
    weightsFakesElUp3b= ("fakeLeptWgt_El_UP", "chargeFlipWgt", "1.0")
    weightsFakesMuUp3b= ("fakeLeptWgt_Mu_UP", "chargeFlipWgt", "1.0")
    weightsFakesFlipUp3b= ("fakeLeptWgt", "chargeFlipWgt_UP", "1.0")

    fakeSample = configWriter.Sample("fakes", ROOT.kOrange - 9)
    fake3bSample = configWriter.Sample("fakes", ROOT.kOrange - 9)
    
    fakeSample.setWeights(weightsFakes)
    fake3bSample.setWeights(weightsFakes3b)
    fakeSample.addSystematic(Systematic("FlipWgt", weightsFakes, weightsFakesFlipUp, weightsFakes, "weight",  "histoSysOneSideSym"))
    fake3bSample.addSystematic(Systematic("FlipWgt", weightsFakes3b, weightsFakesFlipUp3b, weightsFakes3b, "weight",  "histoSysOneSideSym"))

    #Extra systematic to account for fake efficiencies measured with >=1b and applied to >=3b (only for SR3b)
    eeCentralFakesSR3b = 1.3
    eeUncertaintyFakesSR3b = 0.2
    emCentralFakesSR3b = 1.3
    emUncertaintyFakesSR3b = 0.3
    mmCentralFakesSR3b = 1.1
    mmUncertaintyFakesSR3b = 0.5
    nominalWeightStringSR3b = "fakeLeptWgt*({0}*(chanDilep==0) + {1}*(chanDilep==1) + {2}*(chanDilep==2))".format(eeCentralFakesSR3b, emCentralFakesSR3b, mmCentralFakesSR3b)

    uncertaintyUpWeightStringSR3b = "fakeLeptWgt*({0}*(chanDilep==0) + {1}*(chanDilep==1) + {2}*(chanDilep==2))".format(\
        eeCentralFakesSR3b + eeUncertaintyFakesSR3b, emCentralFakesSR3b +  emUncertaintyFakesSR3b, mmCentralFakesSR3b + mmUncertaintyFakesSR3b)
    weightsFakes1bto3bUp= (nominalWeightStringSR3b, "chargeFlipWgt", uncertaintyUpWeightStringSR3b)

    uncertaintyDownWeightStringSR3b = "fakeLeptWgt*({0}*(chanDilep==0) + {1}*(chanDilep==1) + {2}*(chanDilep==2))".format(\
        eeCentralFakesSR3b - eeUncertaintyFakesSR3b, emCentralFakesSR3b -  emUncertaintyFakesSR3b, mmCentralFakesSR3b - mmUncertaintyFakesSR3b)
    weightsFakes1bto3bDown= (nominalWeightStringSR3b, "chargeFlipWgt", uncertaintyDownWeightStringSR3b)

    fake3bSample.addSystematic(Systematic("fakes1bto3b",  weightsFakes3b, weightsFakes1bto3bUp, weightsFakes1bto3bDown, "weight", "histoSys"))
    ############## end extra SR3b fake uncertainty #############
    
    if useComplexFakeUncertainty:
        splitFakeRateSystematicsList = ["MuCorr", "ElCorr"]
        nBinsEl = 20
        for bin in range(nBinsEl):
            splitFakeRateSystematicsList.append("ElUncorrBin"+str(bin))
        nBinsMu = 4
        for bin in range(nBinsMu):
            splitFakeRateSystematicsList.append("MuUncorrBin"+str(bin))
            
        for systematic in splitFakeRateSystematicsList:
            weightsFakesUP = ("fakeLeptWgt_"+systematic+"_UP", "chargeFlipWgt")
            weightsFakesUP3b = ("fakeLeptWgt_"+systematic+"_UP", "chargeFlipWgt", "1.0")
            fakeSample.addSystematic(Systematic("Fakes_"+systematic, weightsFakes, weightsFakesUP, weightsFakes, "weight", "overallSys"))
            fake3bSample.addSystematic(Systematic("Fakes_"+systematic, weightsFakes3b, weightsFakesUP3b, weightsFakes3b, "weight", "overallSys"))
    else:
        fakeSample.addSystematic(Systematic("Fakes_El", weightsFakes, weightsFakesElUp, weightsFakes, "weight",  "histoSysOneSideSym"))
        fake3bSample.addSystematic(Systematic("Fakes_El", weightsFakes3b, weightsFakesElUp3b, weightsFakes3b, "weight",  "histoSysOneSideSym"))
        fakeSample.addSystematic(Systematic("Fakes_Mu", weightsFakes, weightsFakesMuUp, weightsFakes, "weight",  "histoSysOneSideSym"))
        fake3bSample.addSystematic(Systematic("Fakes_Mu", weightsFakes3b, weightsFakesMuUp3b, weightsFakes3b, "weight",  "histoSysOneSideSym"))

    
    fakeSample.setFileList(inputFileDict['fakes'])
    fake3bSample.setFileList(inputFileDict['fakes'])
    if options.useStat=="MergedSamples":
        fakeSample.setStatConfig(True)
        fake3bSample.setStatConfig(True)
    elif options.useStat=="PerSample":
        fakeSample.addSystematic(Systematic("mcstat_fakes", "_nom", "_nom", "_nom", "tree", "shapeStat"))
        fake3bSample.addSystematic(Systematic("mcstat_fakes", "_nom", "_nom", "_nom", "tree", "shapeStat"))


    ## charge mis-Id ##
    weightsMisId=("chargeFlipWgt", "1.0") # python hack
    weightsMisIdUp=("chargeFlipWgt_UP", "1.0") # python hack, issue with tuple of length 1
    misIDSample = configWriter.Sample("chargeFlip", ROOT.kOrange +4)
    misIDSample.setWeights(weightsMisId)
    misIDSample.addSystematic(Systematic("FlipWgt", weightsMisId, weightsMisIdUp, weightsMisId, "weight", "overallSys"))
    misIDSample.setFileList(inputFileDict['misId'])
    if options.useStat=="MergedSamples":
        misIDSample.setStatConfig(True)
    elif options.useStat=="PerSample":
        misIDSample.addSystematic(Systematic("mcstat_misId", "_nom", "_nom", "_nom", "tree", "shapeStat"))

    ################ data ##########
    dataSample = configWriter.Sample("data", ROOT.kBlack)
    dataSample.setData()
    dataSample.setFileList(inputFileDict['data'])

    #------------------#
    # Build fit config #
    #------------------#
    myFitConfig = configMgr.addFitConfig(fcName)

    if options.useStat:
        myFitConfig.statErrThreshold = 0.05 
    else:
        myFitConfig.statErrThreshold = None

    measurement = myFitConfig.addMeasurement(name = "NormalMeasurement", lumi=1.0, lumiErr=0.028)
    measurement.addPOI(my_mu_SIG)

    myFitConfig.addSamples(dataSample)
    for background in backgroundDict:
        if not background == 'diBoson3LWV':
            myFitConfig.addSamples(backgroundDict[background]) # this includes signal
    myFitConfig.addSamples(misIDSample)
    myFitConfig.addSamples(fakeSample)


    if len(sigSample) > 0:          
        myFitConfig.setSignalSample(backgroundDict[signalName])

        #for grid in signalRunnumberToMassesDict:
        grid = options.signalGridName
        if signalName.strip('signal') in signalRunnumberToMassesDict[grid]:
            masses = signalRunnumberToMassesDict[grid][signalName.strip('signal')]
            
        myFitConfig.hypoTestName = 'signal_{0}_{1}'.format(masses[0],masses[1])

        
    #----------------#
    # Build channels #
    #----------------#

    for controlRegion in controlRegionList:
        if controlRegion ==  "CRVV": 
            meffCRVV = myFitConfig.addChannel("meff", ["CRVV"], 5, 150000, 400000)
            meffCRVV.useOverflowBin = True
            myFitConfig.setBkgConstrainChannels(meffCRVV)
        elif controlRegion == "": continue
        else: raise Exception("this CR is not implemented: "+controlRegion)

    ############## starting to define signal regions ###########################
        
    for validationRegionName in validationRegionList:
        legendTitle = ''
        if validationRegionName.endswith('_disc') or (validationRegionName.startswith("SR") and "_bin" in validationRegionName):
            vChan = myFitConfig.addValidationChannel("cuts", [validationRegionName], 1, 0, 2)
            vChan.title = '{0} Region'.format(validationRegionName)
            if validationRegionName.startswith('SR3b'):
                vChan.removeSample("fakes")
                vChan.addSample(fake3bSample)
                if useCoarseGranularity:
                     vChan.removeSample('diBoson')
                     vChan.addSample(diBosonWVSR3b)

        elif validationRegionName.startswith("SR0b"):
            vChan = myFitConfig.addValidationChannel("meff", [validationRegionName], 4, 300000, 1500000)
            vChan.useOverflowBin = True
            vChan.title = '{0} Region'.format(validationRegionName)
    
        elif validationRegionName.startswith("SR1b"):
            vChan = myFitConfig.addValidationChannel("meff", [validationRegionName], 3, 300000, 1500000)
            vChan.useOverflowBin = True
            vChan.title = '{0} Region'.format(validationRegionName)
    
        elif validationRegionName.startswith("SR3b"): 
            vChan = myFitConfig.addValidationChannel("meff", [validationRegionName], 2, 190000, 1500000)
            vChan.useOverflowBin = True
            vChan.title = '{0} Region'.format(validationRegionName)
            if validationRegionName.startswith('SR3b'):
                vChan.removeSample("fakes")
                vChan.addSample(fake3bSample)
                if useCoarseGranularity:
                    vChan.removeSample('diBoson')
                    vChan.addSample(diBosonWVSR3b)

        elif validationRegionName.startswith("SR3Llow"):
            vChan = myFitConfig.addValidationChannel("meff", [validationRegionName],  2, 255000, 1200000)
            vChan.useOverflowBin = True
            vChan.title = '{0} Region'.format(validationRegionName)

        elif validationRegionName.startswith("SR3Lhigh"):
            vChan = myFitConfig.addValidationChannel("meff", [validationRegionName], 2, 355000, 1800000)
            vChan.useOverflowBin = True
            vChan.title = '{0} Region'.format(validationRegionName)

        elif validationRegionName.startswith("VRttW"):
            vChan = myFitConfig.addValidationChannel("meff", [validationRegionName], 3, 100000, 1000000)
            vChan.useOverflowBin = False
            vChan.title = 't #bar{t} + W Validation Region'

        elif validationRegionName.startswith("VRttZ"):    
            vChan = myFitConfig.addValidationChannel("meff", [validationRegionName], 3, 300000, 1000000)
            vChan.useOverflowBin = False
            vChan.title = 't #bar{t} + Z Validation Region'

        elif validationRegionName.startswith("VRttV"):    
            vChan = myFitConfig.addValidationChannel("meff", [validationRegionName], 3, 300000, 1000000)
            vChan.useOverflowBin = False
            vChan.title = 't #bar{t} + V Validation Region'

        elif validationRegionName.startswith("VRVV"):    
            vChan = myFitConfig.addValidationChannel("meff", [validationRegionName], 7, 100000, 800000)
            vChan.useOverflowBin = False
            vChan.title = 'Diboson Validation Region'
        elif validationRegionName == "": continue
        else:
            raise Exception("this validation region is not implemented: "+ validationRegionName)
        
        vChan.titleX = "m_{eff} [GeV]"
        #vChan.titleY = "Entries / {0} GeV".format((xMax-xMin)/nBins)
        vChan.ATLASLabelX = 0.17
        vChan.logY = False
        vChan.ATLASLabelY = 0.915
        vChan.ATLASLabelText = "Internal"
        vChan.showLumi = True

    ############## starting to define signal regions ###########################

    for signalRegionName in signalRegionList:
        if discoveryFit:
            sChan = myFitConfig.addChannel("cuts", [signalRegionName],1, 0, 2)
            if signalRegionName.startswith("SR3b"):
                sChan.removeSample("fakes")
                sChan.addSample(fake3bSample)
                if useCoarseGranularity:
                    sChan.removeSample('diBoson')
                    sChan.addSample(diBosonWVSR3b)
        elif signalRegionName.startswith("SR0b"):
            sChan = myFitConfig.addChannel("meff", [signalRegionName],  4, 300000, 1500000)
        elif signalRegionName.startswith("SR1b"):
            sChan = myFitConfig.addChannel("meff", [signalRegionName], 3, 300000, 1500000)
        elif signalRegionName.startswith("SR3b"):
            sChan = myFitConfig.addChannel("meff", [signalRegionName], 2, 190000, 1500000)
            sChan.removeSample("fakes")
            sChan.addSample(fake3bSample)
            if useCoarseGranularity:
                sChan.removeSample('diBoson')
                sChan.addSample(diBosonWVSR3b)
        elif signalRegionName.startswith("SR3Llow"):
            sChan = myFitConfig.addChannel("meff",[signalRegionName], 2, 255000, 1200000)
        elif signalRegionName.startswith("SR3Lhigh"):
            sChan = myFitConfig.addChannel("meff",[signalRegionName], 2, 355000, 1800000)

        elif signalRegionName == "SRall" or signalRegionName == "SRall_disc":
            sChan = myFitConfig.addChannel("cuts",[signalRegionName],1, 0, 2)
        elif signalRegionName == "": continue
        else:
            raise Exception("ERROR unexpected signal region {0}".format(signalRegionName))

        if discoveryFit or signalRegionName=="SRall" or signalRegionName=="SRall_disc":
            sChan.addDiscoverySamples([signalRegionName], [1.], [0.], [100.], [ROOT.kPink])
        sChan.useOverflowBin = True
        sChan.logY = False
        myFitConfig.setSignalChannels(sChan)
    
    ###################### Setting Style ##############################
        
    # Set global plotting colors/styles
    myFitConfig.dataColor = ROOT.kBlack
    myFitConfig.totalPdfColor = ROOT.kBlack
    myFitConfig.errorFillColor = ROOT.kBlack
    myFitConfig.errorFillStyle = 3004
    #  myFitConfig.errorFillStyle = 3353
    myFitConfig.errorLineStyle = ROOT.kDashed
    myFitConfig.errorLineColor = ROOT.kBlack
    myFitConfig.ShowLumi = True;
    
    c = ROOT.TCanvas()
    compFillStyle = 1001 # see ROOT for Fill styles
    leg = ROOT.TLegend(0.6, 0.48, 0.881, 0.87, "")
    leg.SetFillStyle(0)
    leg.SetFillColor(0)
    leg.SetBorderSize(0)
    leg.SetTextSize(0.035)
    leg.SetTextFont(42)

    entry = ROOT.TLegendEntry()
    if configMgr.blindVR:
        entry = leg.AddEntry("","Toy Data (%.1f fb^{-1}, #sqrt{s}=8 TeV)"%configMgr.outputLumi,"p") 
    else:
        entry = leg.AddEntry("","Data", "p") 
    entry.SetMarkerColor(myFitConfig.dataColor)
    entry.SetMarkerStyle(20)


    if useCoarseGranularity:
        entry = leg.AddEntry("","Diboson + Triboson","f")
        entry.SetLineColor(ROOT.kBlack)
        entry.SetLineWidth(1)
        entry.SetFillColor(backgroundDict['diBoson'].color)
        entry.SetFillStyle(compFillStyle)

        entry = leg.AddEntry("","Top + X","f") 
        entry.SetLineColor(ROOT.kBlack)
        entry.SetLineWidth(1)
        entry.SetFillColor(backgroundDict['topX'].color)
        entry.SetFillStyle(compFillStyle)

    else:
        entry = leg.AddEntry("","Diboson WZ","f")
        entry.SetLineColor(ROOT.kBlack)
        entry.SetLineWidth(1)
        entry.SetFillColor(backgroundDict['diBosonWZ'].color)
        entry.SetFillStyle(compFillStyle)
        
        entry = leg.AddEntry("","Diboson WW","f")
        entry.SetLineColor(ROOT.kBlack)
        entry.SetLineWidth(1)
        entry.SetFillColor(backgroundDict['diBosonSS'].color)
        entry.SetFillStyle(compFillStyle)

        entry = leg.AddEntry("","Diboson ZZ","f")
        entry.SetLineColor(ROOT.kBlack)
        entry.SetLineWidth(1)
        entry.SetFillColor(backgroundDict['diBosonZZ'].color)
        entry.SetFillStyle(compFillStyle)
        
        entry = leg.AddEntry("","VVV","f")
        entry.SetLineColor(ROOT.kBlack)
        entry.SetLineWidth(1)
        entry.SetFillColor(backgroundDict['triBoson'].color)
        entry.SetFillStyle(compFillStyle)

        entry = leg.AddEntry("","t #bar{t} + Z","f") 
        entry.SetLineColor(ROOT.kBlack)
        entry.SetLineWidth(1)
        entry.SetFillColor(backgroundDict['topZ'].color)
        entry.SetFillStyle(compFillStyle)

        entry = leg.AddEntry("","t #bar{t} + W","f") 
        entry.SetLineColor(ROOT.kBlack)
        entry.SetLineWidth(1)
        entry.SetFillColor(backgroundDict['topW'].color)
        entry.SetFillStyle(compFillStyle)

        entry = leg.AddEntry("","single Top + Z","f") 
        entry.SetLineColor(ROOT.kBlack)
        entry.SetLineWidth(1)
        entry.SetFillColor(backgroundDict['singleTopZ'].color)
        entry.SetFillStyle(compFillStyle)

        entry = leg.AddEntry("","Higgs+ttbar","f")
        entry.SetLineColor(ROOT.kBlack)
        entry.SetLineWidth(1)
        entry.SetFillColor(backgroundDict['ttbarHiggs'].color)
        entry.SetFillStyle(compFillStyle)   

        
    entry = leg.AddEntry("","Fake leptons","f")
    entry.SetLineColor(ROOT.kBlack)
    entry.SetLineWidth(1)
    entry.SetFillColor(fakeSample.color)
    entry.SetFillStyle(compFillStyle)
    
    entry = leg.AddEntry("","Charge flip","f")
    entry.SetLineColor(ROOT.kBlack)
    entry.SetLineWidth(1)
    entry.SetFillColor(misIDSample.color)
    entry.SetFillStyle(compFillStyle)

    

    if myFitConfig.signalSample and not myFitConfig.signalSample.startswith("Discovery"):
        if myFitConfig.signalSample=="signal156548":
            sigName="gtt, (gl, #chi^{0}_{1}) = (1000, 1) GeV"
        elif myFitConfig.signalSample=="signal173836":
            sigName="2-steps via slepton, (gl, #chi^{0}_{1}) = (1145, 425) GeV"
        elif myFitConfig.signalSample=="signal172181":
            sigName="sb->top chargino, (sb, #chi^{#pm}_{1}) = (500, 150) GeV"
        elif  myFitConfig.signalSample=="signal148208":
            sigName="1-step x1/2, (#tilde{g}, #chi^{0}_{1}) = (585, 345) GeV"
        elif  myFitConfig.signalSample=="signal177352":
            sigName="#tilde{g} -> #tilde{t} c, (#tilde{g}, #tilde{t}) = (700, 400) GeV"
        elif  myFitConfig.signalSample=="signal174344":
            sigName="#tilde{g} -> #tilde{t} t, (#tilde{g}, #tilde{t}) = (945, 417) GeV"
        elif  myFitConfig.signalSample=="signal173789":
            sigName="2-step via sleptons, (#tilde{g}, #chi^{0}_{1}) = (825, 405) GeV"
        elif  myFitConfig.signalSample=="signal172175":
            sigName="#tilde{b} -> t #chi^{#pm}_{1}, N60, (#tilde{g}, #chi^{#pm}_{1}) = (450, 200) GeV"

        else:
            sigName=myFitConfig.signalSample

        entry = leg.AddEntry("",sigName,"lf")
        sigColor=myFitConfig.getSample(myFitConfig.signalSample).color
        entry.SetLineColor(sigColor)
        entry.SetFillColor(sigColor)
        entry.SetFillStyle(compFillStyle)

    
    myFitConfig.tLegend = leg
    c.Close()
    return

def replaceStringInTuple(inTuple, oldString, newString):
    outTuple = ()
    for entry in inTuple:
        if not entry == oldString:
            outTuple += (entry,)
        else: outTuple += (newString,)
    return outTuple

runAll()
