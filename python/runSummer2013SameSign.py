#!/usr/bin/env python 
# vim: tabstop=8 shiftwidth=4 softtabstop=4 expandtab :

# this will currently only run for Thibaut in Cambridge,
# but can be adapted easily by changing the histFitter Directory
# HistFitterUser must be in same main directory as HistFitter

from pathUtilities import histFitterDir

jobLength = 10 # HACK FIXME

import pathUtilities
isCambridge = pathUtilities.isCambridge()
runIndividualSRHASTOBETRUE = True

import sys
sys.path.append(pathUtilities.histFitterTopDirectory())
excludedPoints = []

import subprocess, os

def main():
    gridList = [
        'Mc12SusyGtt', 
        # 'Mc12SusySMSS2WWZZ',  
        # 'Mc12SusySMGG2WWZZ', 
        # 'Mc12SusyGG2step_slep', 
        # 'Mc12SusyGluinoStop', 
        # 'Mc12SusyGluinoStopCharm', 
        # 'Mc12SusySbottom_TopChargino_NhalfC', 
        # 'Mc12SusySbottom_TopChargino_N60', 
        # 'Mc12SusySMSS2CNsl', 
        # 'Mc12SusyGMSB2d', 
        # 'Mc12SusyRPVUDD',
        # 'Mc12SusyBRPV',
        # 'Mc12SusyMUED',
        # 'Mc12SusyGG1step_x12',
        # 'Mc12SusySMSSonestepCC',
        # 'Mc12SusyMSUGRA30',

        # 'Mc12SusyRPVGluino121',
        # 'Mc12SusyRPVGluino122',
        # 'Mc12SusyRPVGluino133',
        # 'Mc12SusyRPVGluino233',

        ]
    
    runNameTag =  '_20140710TomMessingAround'
    from odict import odict
    extraOptionsDict = odict()
    extraOptionsDict[runNameTag] = '' # for the nomnial case
    
    #makeSRdisributions(extraOptionsDict, True)

    if runIndividualSRHASTOBETRUE:
        # these three will be expected to follow the name convention below.
        # Else, all hell breaks loose
        #extraOptionsDict['_SR0bEE'+runNameTag] = '--signalRegions SR0bee'
        #extraOptionsDict['_SR0bEM'+runNameTag] = '--signalRegions SR0bem'
        #extraOptionsDict['_SR1bEE'+runNameTag] = '--signalRegions SR1bee'
        #extraOptionsDict['_SR1bEM'+runNameTag] = '--signalRegions SR1bem'
        #extraOptionsDict['_SR3LhighEE'+runNameTag] = '--signalRegions SR3Lhighee'
        #extraOptionsDict['_SR3LlighEM'+runNameTag] = '--signalRegions SR3Lhihgem'
        
        extraOptionsDict['_0bjetOnly'+runNameTag] = '--signalRegions SR0b'
        extraOptionsDict['_1bjetOnly'+runNameTag] = '--signalRegions SR1b'
        extraOptionsDict['_3bjetOnly'+runNameTag] = '--signalRegions SR3b'
        extraOptionsDict['_3LlowOnly'+runNameTag] = '--signalRegions SR3Llow'
        extraOptionsDict['_3LhighOnly'+runNameTag] = '--signalRegions SR3Lhigh'
        
    
    gridPointDict = makeGridPointDict(gridList)

    
    if isCambridge:
        # TOM: this runs with asymptotics
        runInCambridgeWithTheKraken(gridPointDict, extraOptionsDict)

        # TOM: Plotting boolean arguments: makeList, makeHist, makePlot
        runPlottingSequentially(gridList, extraOptionsDict, True, True, True)
        # runPlottingInCambridgeWithTheKraken(gridList, extraOptionsDict, True, True, True)
    else :
        runSequentially(gridPointDict, extraOptionsDict)
        runPlottingSequentially(gridList, extraOptionsDict, True, True, True) 
        


def generateHistFitterCommands(gridPointDict, extraOptionsDict):
    commandsList = []
    mergeCommandDict = {}
    if isCambridge:
        setupCommand =  'cd {0}; . ./setup_histFitter.sh; '.format(histFitterDir)
    else:
        setupCommand =  'cd {0};. setup.sh; cd ../HistFitterUser/MET_jets_2lep_SS; '.format(histFitterDir)
    
    for grid in gridPointDict:
        exclMode = 'p'
        for theoryUncert in ['nom', 'up', 'down']:
            for extraOption in extraOptionsDict:
                if not ((theoryUncert == 'up' or theoryUncert == 'down') and 'Only' in extraOption): #excludes up/down for bJetOnly
                    mergeCommandDict[grid+theoryUncert+extraOption+'hypotest'] = '{0} hadd -f results/{1}_{2}{3}_summer2013SameSign_output_hypotest.root '.format(setupCommand, grid, theoryUncert, extraOption)
                    mergeCommandDict[grid+theoryUncert+extraOption+'upperlimit'] = '{0} hadd -f results/{1}_{2}{3}_summer2013SameSign_output_upperlimit.root '.format(setupCommand, grid, theoryUncert, extraOption)                    
                    index = 0
                    for subListConcat in yieldSubList(gridPointDict[grid][theoryUncert], jobLength):
                        index += 1 # to differentiate output
                        if isCambridge:
                            outNameStem = theoryUncert+extraOption+'_temp'+str(index)
                        else:
                            outNameStem = theoryUncert+extraOption
                        if theoryUncert == 'nom' and not 'Only' in extraOption: 
                            exclMode = 'pl'
                        commandsList.append('{0} HistFitter.py -twf -u " --signalGridName {1} --additionalOutName {5} --signalList {2}  --theoryUncertMode {3} {4}" python/summer2013SameSign.py; HistFitter.py -{6} -u " --signalGridName {1}  --additionalOutName {5} --signalList {2}  --theoryUncertMode {3} {4}"  python/summer2013SameSign.py;'.format(setupCommand, grid, subListConcat, theoryUncert, extraOptionsDict[extraOption], outNameStem, exclMode))  
                        mergeCommandDict[grid+theoryUncert+extraOption+'hypotest'] += 'results/{0}_{1}_summer2013SameSign_output_hypotest.root '.format(grid, outNameStem)
                        mergeCommandDict[grid+theoryUncert+extraOption+'upperlimit'] += 'results/{0}_{1}_summer2013SameSign_output_upperlimit.root '.format(grid, outNameStem)

    if isCambridge:
        return commandsList, mergeCommandDict
    else:
        return commandsList


def runSequentially(gridPointDict, extraOptionsDict):
    commandsList = generateHistFitterCommands(gridPointDict, extraOptionsDict)
    for command in commandsList:
        print command
        subprocess.call(command, shell=True)


def runPlottingSequentially(gridList, extraOptionsDict, makeList=True, makeHist=True, makePlot=True):
    os.chdir(os.path.join(pathUtilities.histFitterTopDirectory(), 'macroSS'))
    makeListCommands, makeHistCommands, makePlotCommands = generatePlottingCommands(gridList, extraOptionsDict)
    if makeList:
        for command in makeListCommands:
            subprocess.call(command, shell=True)
    if makeHist:
        for command in makeHistCommands:
            subprocess.call(command, shell=True)
    if makePlot:
        for command in makePlotCommands:
            print
            print command
            print
            subprocess.call(command, shell=True)
    


def generatePlottingCommands(gridList, extraOptionsDict):
    import os
    makeListCommands = []
    # FIXME this will probably need to change
    makeHistCommands = ['python addMassesToUpperLimitTextFiles.py '+extraOptionsDict.keys()[0]+'; ']
    makePlotCommands = []

    import axisLabelsProcessDescriptions
    labelsPerGrid = axisLabelsProcessDescriptions.getLabelsPerGrid()
    
    os.chdir(os.path.join(pathUtilities.histFitterTopDirectory(), 'macroSS')) # such that the output is in the correct folder
    for grid in gridList:
        for extraOption in extraOptionsDict:
            plotInputFilesCommand = ''
            # this is to implement a script by OJ to run the upper limits
            if  not 'bjetOnly' in extraOption:
                upperLimitFileCommand = './summer2013AddXSec.py {0}_nom{1}_summer2013SameSign_output_upperlimit__1_harvest_list_updated'.format(grid, extraOption)
            for theoryUncert in ['nom', 'up', 'down']:
                if not ((theoryUncert == 'up' or theoryUncert == 'down') and 'Only' in extraOption):
                    makeListCommands.append("./runMacroCommand.py \".x summer2013MakeListFiles.C(\"../results/{0}_{1}{2}_summer2013SameSign_output_hypotest.root\")\"".format(grid, theoryUncert, extraOption))
                    if theoryUncert == 'nom' and not 'Only' in extraOption:
                        makeListCommands.append("./runMacroCommand.py \".x summer2013MakeListFiles.C(\"../results/{0}_{1}{2}_summer2013SameSign_output_upperlimit.root\")\"".format(grid, theoryUncert, extraOption))
                        
                    # this is about the ugliest workaround in history
                    # but it somehow seems to break if one runs several instances of the makeContourHists.C in the same ROOT instance
                    # even interactively. This should proably not be the case.
                    makeHistCommands.append("./runMacroCommand.py \".x ../macroSS/summer2013MakeContourHists.C(\"{0}_{1}{2}_summer2013SameSign_output_hypotest__1_harvest_list\")\"".format(grid, theoryUncert, extraOption))
                    plotInputFilesCommand +='\"{0}_{1}{2}_summer2013SameSign_output_hypotest__1_harvest_list.root\", '.format(grid, theoryUncert, extraOption)

            plotInputFilesCommand += '\"{0}_nom{1}_summer2013SameSign_output_upperlimit__1_harvest_list_updated\"'.format(grid, extraOption)
            
            if runIndividualSRHASTOBETRUE and extraOption != 'Only': # only do this for the nominal case for now.
                plotInputFilesCommand += ', '+appendIndividualSRCommand(grid, extraOptionsDict)
            else:
                plotInputFilesCommand += ', None, None, None, None, None'

            # do not plot the individual SRs
            if not 'Only' in extraOption:
                # an ugly hack to get the handing over from python to root to work.
                # Apologies to future Thibaut as well as anyone else who has to look at this
                labels = '{0}, YAXIS{1}YAXIS, PROCESS{2}PROCESS, {3}, {4}, {5}, FORBIDDENLABEL{6}FORBIDDENLABEL'.format(labelsPerGrid[grid].xLabel, labelsPerGrid[grid].yLabel, labelsPerGrid[grid].processDescription,\
                                                                 labelsPerGrid[grid].forbiddenCut, labelsPerGrid[grid].forbiddenLabelXVal, labelsPerGrid[grid].forbiddenLabelYVal, labelsPerGrid[grid].forbiddenLabelText)
                makePlotCommands.append("{2}; ./runMacroCommand.py \".x summer2013MakeContourPlots.C({0}, {1})\"".format(plotInputFilesCommand, labels, upperLimitFileCommand ))
    return makeListCommands, makeHistCommands, makePlotCommands




# this is only tested for use in Cambridge and should not be used otherwise
def runInCambridgeWithTheKraken(gridPointDict, extraOptionsDict):
    commandsList, mergeCommandDict = generateHistFitterCommands(gridPointDict, extraOptionsDict)
    
    for command in commandsList:
        print command
        
    from gillam.distributedExecute import DistributedExecute
    
    if len(commandsList) > 0:
        dist = DistributedExecute(commandsList, 2)
        dist.runAndWait()
        
    mergingCommands = []
    for setting in mergeCommandDict:
        #print mergeCommandDict[setting]
        #subprocess.call(mergeCommandDict[setting], shell=True)
        mergingCommands.append(mergeCommandDict[setting])
        
    if len(mergingCommands) > 0:
        dist = DistributedExecute(mergingCommands, 2)
        dist.runAndWait()        

    #subprocess.call('rm -rf ../results/*_temp*', shell=True)
    #subprocess.call('rm -rf ../data/*_temp*', shell=True)



def runPlottingInCambridgeWithTheKraken(gridList, extraOptionsDict, makeList=True, makeHist=True, makePlot=True):
    makeListCommands, makeHistCommands, makePlotCommands = generatePlottingCommands(gridList, extraOptionsDict)
    
    setupCommand = 'cd {0}; . ./setup_histFitter.sh; cd macroSS; '.format(histFitterDir)
    
    makeListCommandsCambridge = [setupCommand+x for x in makeListCommands]
    makeHistCommandsCambridge = [setupCommand+x for x in makeHistCommands]
    makePlotCommandsCambridge = [setupCommand+x for x in makePlotCommands]

    
    from gillam.distributedExecute import DistributedExecute
    if len(makeListCommandsCambridge) > 0 and makeList:
        print makeListCommandsCambridge
        dist = DistributedExecute(makeListCommandsCambridge, 2)
        dist.runAndWait()
        # TOM : run one appropriate command manually to ensure we have the m0:m12 parameterisation in the
        # generated macroSS/summary_harvest_tree_description file
        for command in makeListCommandsCambridge:
            if 'upperlimit' in command:
                continue
            subprocess.call(command, shell=True)
            break

        
    if makeHist:
        for command in makeHistCommandsCambridge:
            print
            print
            print
            print
            print command
            print
            print
            print
            print
            subprocess.call(command, shell=True)

    if len(makePlotCommandsCambridge) > 0 and makePlot:
        dist = DistributedExecute(makePlotCommandsCambridge, 2)
        dist.runAndWait()


# Smaller Utils
def makeGridPointDict(gridList):
    gridDict = {}

    for grid in gridList:
        gridDict[grid] = {}
        for theoryUncert in ['nom', 'up', 'down']:
            gridDict[grid][theoryUncert] = ''
            gridFile = open(os.path.join(histFitterDir, 'HistFitterUser/MET_jets_2lep_SS/susyGridFiles/', grid))
            for point in gridFile:
                if point.startswith('mc12') and not (point.strip() in excludedPoints):
                    gridDict[grid][theoryUncert] += 'signal'+point.split('.')[1]+';'
            gridDict[grid][theoryUncert] = gridDict[grid][theoryUncert].strip(';')
        gridFile.close()
    return gridDict

def appendIndividualSRCommand(grid, extraOptionsDict):
    singleSRNames = ''
    for option in extraOptionsDict:#['_0bjetOnly', '_1bjetOnly', '_3bjetOnly', '_3LlowOnly', '_3LhighOnly']:
        if 'Only' in option:
            singleSRNames +='\"{0}_{1}{2}_summer2013SameSign_output_hypotest__1_harvest_list.root\", '.format(grid, 'nom', option)
    #singleSRNames += 'None, None'
    return singleSRNames.strip(', ')

def yieldSubList(points, length):
    # the length determines the number of points in each subjob.
    pointsList = points.split(';')
    for subList in [pointsList[x:x+length] for x in range(0,len(pointsList), length)]:
        yield ';'.join(subList)

def makeSRdistributions(extraOptionsDict, runSequentially = False):
    commandsList = []
    for extraOption in extraOptionsDict:
        if not 'Only' in extraOption:
            outNameStem = extraOption+'_plots'
            setupCommand = 'cd {0}; . ./setup_histFitter.sh; '.format(histFitterDir)
            commandsList.append('{0} HistFitter.py -tw -u "--additionalOutName {1} {2}" python/summer2013SameSign.py;'.format(setupCommand, outNameStem, extraOptionsDict[extraOption]))

    if runSequentially:
        for command in commandsList:
            print command
            subprocess.call(command, shell=True)
    else:
        from gillam.distributedExecute import DistributedExecute
        if len(commandsList) > 0:
            dist = DistributedExecute(commandsList, 3)
            dist.runAndWait()



if __name__ == '__main__':
    main()
