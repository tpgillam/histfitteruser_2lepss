histFitterDir = '/usera/gillam/histFitterJuly2014/'

def histFitterTopDirectory():
    if isCambridge():
        return '/usera/gillam/histFitterJuly2014/HistFitterUser/MET_jets_2lep_SS/'
    else:
        #return '/afs/cern.ch/user/o/oducu/WorkArea/SS_HistFitter/HistFitterUserTrunk/MET_jets_2lep_SS/'
        return '/afs/cern.ch/user/c/cote/susy0/users/cote/HistFitterUser6/MET_jets_2lep_SS/'

def currentTreeDirectory():
    if isCambridge():
        return '/r03/atlas/gillam/histFitterRPV/20130812RPV-linkCombo20140301-newCFnewttH'
        #return '/r02/atlas/gillam/histFitterRPV/20130812RPV-linkCombo20140301-newCFoldttH'#20130812RPV-treeProd48/'
    else:
        return '/afs/cern.ch/atlas/groups/susy/2lepSS/20140303_full/'

def isCambridge():
    import subprocess
    return 'cam.ac.uk' in subprocess.Popen('hostname -d', stdout=subprocess.PIPE, shell=True).communicate()[0]
