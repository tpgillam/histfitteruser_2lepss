################################################################
## In principle all you have to setup is defined in this file ##
################################################################
from configManager import configMgr
from ROOT import kBlack,kWhite,kGray,kRed,kPink,kMagenta,kViolet,kBlue,kAzure,kCyan,kTeal,kGreen,kSpring,kYellow,kOrange
from configWriter import TopLevelXML,Measurement,ChannelXML,Sample
from systematic import Systematic
from math import sqrt

### DISCOVERY FIT SETUP ###

from optparse import OptionParser
inputParser = OptionParser()
inputParser.add_option('', '--signalRegionIndex', dest='signalRegionIndex', default=0, help='index of SR to use')
inputParser.add_option('', '--tableFile', dest='tableFile', default='/usera/gillam/histFitterJuly2013/HistFitterUser/MET_jets_2lep_SS/MyYieldsTableSR.pickle', help='which signal region to use')
configArgs = []
userArgs= configMgr.userArg.split(' ')
for i in userArgs:
    configArgs.append(i)
(options, args) = inputParser.parse_args(configArgs)

import pickle
try:
    yieldsTable = pickle.load(open(options.tableFile))
    i = int(options.signalRegionIndex)
    ndata = yieldsTable['nobs'][i]
    nbkg = yieldsTable['TOTAL_FITTED_bkg_events'][i]
    nbkgErr = yieldsTable['TOTAL_FITTED_bkg_events_err'][i]
    print "Reading numbers from %s"%options.tableFile
except:
    ndata     =  14
    nbkg      =  4.17
    nbkgErr   =  1.17
    print "Reading hard-coded numbers in MyUserAnalysis.py"
    pass
print "ndata:",ndata
print "bkg %.2f +- %.2f \n"%(nbkg,nbkgErr)


# Set uncorrelated systematics for bkg and signal (1 +- relative uncertainties)
ucb = Systematic("ucb", configMgr.weights, (nbkg+nbkgErr)/nbkg, (nbkg-nbkgErr)/nbkg, "user","userOverallSys")

# Setting the parameters of the hypothesis test
configMgr.nTOYs=3000
configMgr.calculatorType=2 # 2=asymptotic calculator, 0=frequentist calculator
configMgr.testStatType=3   # 3=one-sided profile likelihood test statistic (LHC default)
configMgr.nPoints=20       # number of values scanned of signal-strength for upper-limit determination of signal strength.

##########################

# Give the analysis a name
configMgr.analysisName = "MyUserAnalysis"
configMgr.outputFileName = "results/%s_Output.root"%configMgr.analysisName

# Define cuts
configMgr.cutsDict["Sig"] = "1."

# Define weights
configMgr.weights = "1."

# Define samples
bkgSample = Sample("Bkg",kGreen-9)
bkgSample.setStatConfig(False)
bkgSample.buildHisto([nbkg],"Sig","cuts") 
bkgSample.addSystematic(ucb)

dataSample = Sample("Data",kBlack)
dataSample.setData()
dataSample.buildHisto([ndata],"Sig","cuts")

# Define top-level
ana = configMgr.addFitConfig("SPlusB")
ana.addSamples([bkgSample,dataSample])



# Define measurement
meas = ana.addMeasurement(name="NormalMeasurement",lumi=1.0,lumiErr=0.036)
meas.addPOI("mu_Sig")
meas.addParamSetting("Lumi","const",1.0)

# Create channel and add discovery sample
chan = ana.addChannel("cuts",["Sig"],1,0.,1.)
chan.addDiscoverySamples(["Sig"],[1.],[0.],[100.],[ROOT.kPink])
ana.setSignalChannels([chan])


# These lines are needed for the user analysis to run
# Make sure file is re-made when executing HistFactory
if configMgr.executeHistFactory:
    if os.path.isfile("data/%s.root"%configMgr.analysisName):
        os.remove("data/%s.root"%configMgr.analysisName) 
