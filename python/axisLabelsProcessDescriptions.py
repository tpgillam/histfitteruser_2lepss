#!/usr/bin/env python
import sys
sys.path.append('python') #this is a hack to import the sameSignTools from /python
import os, pathUtilities
dataDir = os.path.join(pathUtilities.histFitterTopDirectory(), 'susyGridFiles')

#def main():
#    getLabelsPerGrid()

def getLabelsPerGrid():
    import os, sys
    labelsPerGrid = {}
    for file in os.listdir(dataDir):
        if file.startswith('Mc12Susy') and not file == 'Mc12Susy_missingPoints'\
               and not file.endswith('~') and not file.endswith('.root') and not os.path.isdir(os.path.join(dataDir,file)):
            labelsPerGrid[file] = sameSignLabelsAndAxes(file)
            labelsPerGrid[file].getLabels()
    return labelsPerGrid

class sameSignLabelsAndAxes():
    def __init__(self, gridName):
        self.gridName = gridName
        self.processDescription = ""
        self.xLabel = ""
        self.yLabel = ""

        self.forbiddenCut = 0
        self.forbiddenLabelXVal = 0
        self.forbiddenLabelYVal = 0
        self.forbiddenLabelText = "None"

    def getLabels(self):
        processDescriptionAndAxisLabels = getProcessDescriptionAndAxisLabels()
        forbiddenCutsAndLabels = getForbiddenCutsAndLabels()
        for grid in processDescriptionAndAxisLabels:
            if grid == self.gridName:
                self.xLabel = processDescriptionAndAxisLabels[grid][0]
                self.yLabel = processDescriptionAndAxisLabels[grid][1]
                self.processDescription = processDescriptionAndAxisLabels[grid][2]

                if grid in forbiddenCutsAndLabels:
                    self.forbiddenCut = forbiddenCutsAndLabels[grid][0]
                    self.forbiddenLabelXVal = forbiddenCutsAndLabels[grid][1]
                    self.forbiddenLabelYVal = forbiddenCutsAndLabels[grid][2]
                    self.forbiddenLabelText = forbiddenCutsAndLabels[grid][3]

def getProcessDescriptionAndAxisLabels():
    processDescriptionAndAxisLabels = {
    'Mc12SusyGtt': [
        'm_{#tilde{g}} [GeV]',
        'm_{#tilde{#chi}^{0}_{1}} [GeV]',
        '#tilde{g} #tilde{g} production, #tilde{g}#rightarrow t#bar{t}#tilde{#chi}^{0}_{1}, m(#tilde{t_{1}}) >> m(#tilde{g})'],
    'Mc12SusyGG1step_lsp60':[
        "m_{#tilde{g}} [GeV]",
        "x = (m_{#tilde{#chi}^{#pm}_{1}} - m_{#tilde{#chi}^{0}_{1}})/(m_{#tilde{g}}-m_{#tilde{#chi}^{0}_{1}})",
        "#tilde{g} #tilde{g} #rightarrow qqqqWW #tilde{#chi}^{0}_{1}#tilde{#chi}^{0}_{1}, m(#tilde{#chi}^{0}_{1}) = 60 GeV"],
    'Mc12SusyGtb':[
        'm_{#tilde{g}} [GeV]',
        'm_{#tilde{#chi}^{0}_{1}} [GeV]',
        "#tilde{g} #tilde{g} production, #tilde{g}#rightarrow tb+#tilde{#chi}^{0}_{1}, m(#tilde{t_{1}}) >> m(#tilde{g}), m(#tilde{#chi}^{0}_{1}) #approx m(#tilde{#chi}^{#pm}_{1})"],
    'Mc12SusySMSS2WWZZ':[
        'm_{#tilde{q}} [GeV]',
         "m_{#tilde{#chi}^{0}_{1}} [GeV]",
        "#tilde{q} #tilde{q} production, 2-step decay: #tilde{q} #tilde{q} #rightarrow q'q'WZWZ#tilde{#chi}^{0}_{1}#tilde{#chi}^{0}_{1}"],
    'Mc12SusySMGG2WWZZ':[
        'm_{#tilde{g}} [GeV]',
        'm_{#tilde{#chi}^{0}_{1}} [GeV]',
        "#tilde{g} #tilde{g} production, 2-step decay: #tilde{g} #tilde{g} #rightarrow qqq'q'WZWZ#tilde{#chi}^{0}_{1}#tilde{#chi}^{0}_{1}"],
    'Mc12SusyGG1step_x12':[
        "m_{#tilde{g}} [GeV]",
        "m_{#tilde{#chi}^{0}_{1}} [GeV]",
        "#tilde{g} #tilde{g} #rightarrow qqq'q'WW #tilde{#chi}^{0}_{1}#tilde{#chi}^{0}_{1}, m(#tilde{#chi}^{#pm}_{1}) = 2 m(#tilde{#chi}^{0}_{1})"],
    'Mc12SusyGG2step_slep':[
        "m_{#tilde{g}} [GeV]",
        'm_{#tilde{#chi}^{0}_{1}} [GeV]',
        "#tilde{g} #tilde{g} decays via sleptons, #tilde{g} #tilde{g} #rightarrow qqq'q'll(ll)#tilde{#chi}^{0}_{1} #tilde{#chi}^{0}_{1} + neutrinos"],
    'Mc12SusyGttOnShell': [
        'm_{#tilde{g}} [GeV]',
        'm_{#tilde{t_{1}}} [GeV]',
        '#tilde{g} #tilde{g} production, #tilde{g}#rightarrow t#bar{t}#tilde{#chi}^{0}_{1}, m(#tilde{t_{1}}) < m(#tilde{g}), m(#tilde{#chi}^{0}_{1}) = 60 GeV'],
    'Mc12SusyGluinoStop':[
        "m_{#tilde{g}} [GeV]",
        "m_{#tilde{t_{1}}} [GeV]",
        " #tilde{g}#rightarrow t#tilde{t_{1}}, #tilde{t_{1}} #rightarrow b#tilde{#chi}^{#pm}_{1}, m(#tilde{t_{1}}) < m(#tilde{g}), m(#tilde{#chi}^{0}_{1}) = 60 GeV, #tilde{#chi}^{#pm}_{1} = 118 GeV"],
    'Mc12SusyGluinoStopCharm':[
        'm_{#tilde{g}} [GeV]',
        "m_{#tilde{t_{1}}} [GeV]",
        "#tilde{g} #tilde{g} production, #tilde{g}#rightarrow tc+#tilde{#chi}^{0}_{1}, m(#tilde{#chi}^{0}_{1}) = m(#tilde{t_{1}}) - 20 GeV"],
    'Mc12SusySbottom_TopChargino_NhalfC':[
        "m_{#tilde{b_{1}}} [GeV]",
        "m_{#tilde{#chi}^{0}_{1}} [GeV]",
        "#tilde{b_{1}} #tilde{b_{1}} production, #tilde{b_{1}}#rightarrow t#tilde{#chi}^{#pm}_{1}, m(#tilde{#chi}^{#pm}_{1}) = 2 m(#tilde{#chi}^{0}_{1})"],
    'Mc12SusySMSS2CNsl':[
        'm_{#tilde{q}} [GeV]',
        'm_{#tilde{#chi}^{0}_{1}} [GeV]',
        "#tilde{q} #tilde{q} decays via sleptons, #tilde{q} #tilde{q} #rightarrow q'q'll(ll)#tilde{#chi}^{0}_{1} #tilde{#chi}^{0}_{1} + neutrinos"],
    'Mc12SusySbottom_TopChargino_N60':[
        "m_{#tilde{b_{1}}} [GeV]",
        "m_{#tilde{#chi}^{#pm}_{1}} [GeV]",
         "#tilde{b_{1}} #tilde{b_{1}} production, #tilde{b_{1}}#rightarrow t#tilde{#chi}^{#pm}_{1}, m(#tilde{#chi}^{0}_{1}) = 60 GeV"],
    'Mc12SusySbottom_TopChargino_N60_alternateX': [
        "m_{#tilde{b}} [GeV]",
        "x = (m_{#tilde{#chi}^{#pm}_{1}} - m_{#tilde{#chi}^{0}_{1}})/(m_{#tilde{b}}-m_{#tilde{#chi}^{0}_{1}}) [GeV]",
        "#tilde{b_{1}} #tilde{b_{1}} production, #tilde{b_{1}}#rightarrow t#tilde{#chi}^{#pm}_{1}, m(#tilde{#chi}^{0}_{1}) = 60 GeV"],
    'Mc12SusyMSUGRA30': [
        "m_{0} [GeV]",
        "m_{1/2} [GeV]",
        "MSUGRA/CMSSM: tan(#beta)=30, A_{0} = -2m_{0}, #mu > 0"],
    'Mc12SusyGMSBStop1':[
        "m_{#tilde{t_{1}}} [GeV]",
        "#tilde{#chi}^{0}_{1} [GeV]",
        "direct #tilde{t_{1}}-#tilde{t_{1}} production, GMSB model"],
    'Mc12SusyDirectStop2':[
        "m_{#tilde{t_{2}}} [GeV]",
        "m_{#tilde{#chi}^{0}_{1}} [GeV]",
        "#tilde{t_{2}} #tilde{t_{2}} production, #tilde{t_{2}}#rightarrow #tilde{t_{1}}Z, #tilde{t_{1}} #rightarrow #tilde{#chi}^{0}_{1}t"],
    'Mc12SusyRPVUDD':[
        "m_{#tilde{g}} [GeV]",
        "m_{#tilde{t_{1}}} [GeV]",
        "#tilde{g} #tilde{g} production, #tilde{g}#rightarrow #tilde{t_{1}}t, #tilde{t_{1}} (RPV)#rightarrow bs"],
     'Mc12SusySgluon':[
        "arbitrary",
        "m_{#tilde{g}} [GeV]",
        "#tilde{g} #tilde{g} production, #tilde{g}#rightarrow t#bar{t} t#bar{t}"],
    'Mc12SusyGMSB2d':[
       "#Lambda [TeV]",
        "tan(#beta)",
        "GMSB: M_{mess}=250 TeV, N_{5}=3, #mu >0, C_{grav}=1"],
    
       'Mc12SusyBRPV':[
       "m_{0} [GeV]",
        "m_{1/2} [GeV]",
        "MSUGRA/CMSSM: tan(#beta)=30, A_{0} = -2m_{0}, #mu > 0, bRPV"],
    'Mc12SusyPMSSM2':[
       "m_{qL3} [GeV]",
        "#mu [GeV]",
       "pMSSM2"],
    'Mc12SusyMUED':[
            "1/R [GeV]",
            "#Lambda R",
            "Universal Extra Dimension"],
    'Mc12SusyNGM400': [
            'm_{#tilde{t_{1}}}',
            'm_{#tilde{#tau}}',
            'sbottom decaying via staus, mu = 400'],
    'Mc12SusyNGM1000':[
            'm_{#tilde{t_{1}}}',
            'm_{#tilde{#tau}}',
            'sbottom decaying via staus, mu 1000'],
    'Mc12SusyRPVGluino121':[
            'm_{#tilde{g}}',
            'm_{#tilde{#chi}^{0}_{1}}',
            'PRV Gluino LSP, #lambda_{121}'],
    'Mc12SusyRPVGluino122':[
            'm_{#tilde{g}}',
            'm_{#tilde{#chi}^{0}_{1}}',
            'PRV Gluino LSP, #lambda_{122}'],
    'Mc12SusyRPVGluino133':[
            'm_{#tilde{g}}',
            'm_{#tilde{#chi}^{0}_{1}}',
            'PRV Gluino LSP, #lambda_{133}'],
    'Mc12SusyRPVGluino233': [
            'm_{#tilde{g}}',
            'm_{#tilde{#chi}^{0}_{1}}',
            'PRV Gluino LSP, #lambda_{233}'],
    }
    return processDescriptionAndAxisLabels


def getForbiddenCutsAndLabels():
    forbiddenCutsAndLabels = {
    'Mc12SusyGtt': [2*175,
                    550, 240, "m_{#tilde{g}} <  2*m_{t} + m_{#tilde{#chi}^{0}_{1}} "],
    #'Mc12SusyGG1step_lsp60',
    'Mc12SusyGtb':[175+4,
                   450, 300,"m_{#tilde{g}} < m_{t} + m_{b} + m_{#tilde{#chi}^{0}_{1}}"],
    'Mc12SusySMSS2WWZZ':[0, 
                         520, 535, "m_{#tilde{q}} < m_{#tilde{#chi}^{0}_{1}}"],
    'Mc12SusySMGG2WWZZ':[0, 
                         570, 600,"m_{#tilde{g}} < m_{#tilde{#chi}^{0}_{1}}"],
    'Mc12SusyGG1step_x12':[0,
                           400, 430,"m_{#tilde{g}} < m_{#tilde{#chi}^{0}_{1}}"],
    #'Mc12SusySMSSonestepCC',
    'Mc12SusyGG2step_slep':[0,
                            450, 482,"m_{#tilde{g}} < m_{#tilde{#chi}^{0}_{1}}"],
    'Mc12SusyGttOnShell': [175,
                           735, 585, "m(#tilde{g}) < m(#tilde{t_{1}}) + m(t)"],
    'Mc12SusyGluinoStop':[175,
                          730, 595,"m_{#tilde{g}} < m_{t} + m_{#tilde{t_{1}}} "],
    'Mc12SusyGluinoStopCharm':[175 + 25,
                          490, 320,"m_{#tilde{g}} < m_{t} + m_{c} + m_{#tilde{#chi}^{0}_{1}} "],
    #'Mc12SusySbottom_TopChargino_NhalfC':[175,
     #                                    400, 115, "m_{#tilde{b}} <  m_{t} + m_{#tilde{#chi}^{#pm}_{1} }"],
    'Mc12SusySMSS2CNsl':[0,
                         350, 370,"m_{#tilde{q}} < m_{#tilde{#chi}^{0}_{1}}"],
    'Mc12SusySbottom_TopChargino_N60':[175,
                                       370, 210, "m_{#tilde{b}} <  m_{t} + m_{#tilde{#chi}^{#pm}_{1} }"],
    #'Mc12SusySbottom_TopChargino_N60_alternateX',
    #'Mc12SusyMSUGRA30',
    'Mc12SusyGMSBStop1':[4,
                           350, 365,"m_{#tilde{t_{1}}} < m_{b} + m_{#tilde{#chi}^{#pm}_{1}}"],
    'Mc12SusyDirectStop2':[175+90,
                           350, 100," m_{#tilde{t_{2}}} < m_{Z} + m_{#tilde{t_{1}}}"],
    #'Mc12SusyRPVUDD',
    'Mc12SusyRPVGluino121':[0,
                            750, 800,"m_{#tilde{g}} < m_{#tilde{#chi}^{0}_{1}}"],
    'Mc12SusyRPVGluino122':[ 0, 
                             750, 800,"m_{#tilde{g}} < m_{#tilde{#chi}^{0}_{1}}"],
    'Mc12SusyRPVGluino133':[ 0,
                             750, 800,"m_{#tilde{g}} < m_{#tilde{#chi}^{0}_{1}}"],
    'Mc12SusyRPVGluino233': [0,
                             750, 800,"m_{#tilde{g}} < m_{#tilde{#chi}^{0}_{1}}"],

     }
    return forbiddenCutsAndLabels

#main()
