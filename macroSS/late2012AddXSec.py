#!/usr/bin/env python
import os,sys

def retrieveBasePath():
    pwd = os.getcwd()
    pos=pwd.find('MET_jets_2lep_SS/')
    if pos<0:
        raise NameError('Unable to find HistFitterUser path')
    basepath=pwd[:pos+len('MET_jets_2lep_SS/')]
    print 'Setting base path to %s'%(basepath)
    return basepath 

basePath=retrieveBasePath()
sys.path.append(basePath+'/python')
import pathUtilities, signalSameSignTools


def late2012AddXSec():
    if len(sys.argv)<2:
        print 'ERROR: should supply harvest file name'
        return

    col_type='Channel'
    #col_type='Masses'
    
    harvestfile=sys.argv[1]
    '''
    if 'Mc12SusyGtt_' in harvestfile: massesDict = signalSameSignTools.getRunnumberMassesDictSS('Mc12SusyGtt')
    elif 'Mc12SusySbottom_TopChargino_N60_' in harvestfile: massesDict = signalSameSignTools.getRunnumberMassesDictSS('Mc12SusySbottom_TopChargino_N60')
    elif 'SusyGG2step_slep_' in harvestfile: massesDict = signalSameSignTools.getRunnumberMassesDictSS('Mc12SusyGG2step_slep')
    elif 'SusySMSS2CNsl_' in harvestfile: massesDict = signalSameSignTools.getRunnumberMassesDictSS('Mc12SusySMSS2CNsl')
    '''
    gridName = ''
    for string in harvestfile.split('_'):
        if string != 'nom':
            gridName += string+'_'
        else:
            gridName = gridName.strip('_')
            break
    massesDict = signalSameSignTools.getRunnumberMassesDictSS(gridName)
    print gridName
    
    xsecDict={}
    fxsec = open(basePath+'share/crossSectionInfo','r')
    buffer = fxsec.read().splitlines()
    for line in buffer:
        line=line.strip()
        if len(line)>10 and not line.startswith('#'):
            tokens = line.split()
            xsecDict[int(tokens[0])]=float(tokens[1])
            pass

    fin = open(harvestfile,'r')
    buffer = fin.read().splitlines()
    fin.close()
    fout = open(harvestfile,'w')
    for line in buffer:
        if len(line)>10:
            if col_type=='Masses':
                m1 = float(line.split()[-2])
                m2 = float(line.split()[-1])
                ul = float(line.split()[-11])
                for k in massesDict:
                    if abs(float(massesDict[k][0])-m1)<1. and abs(float(massesDict[k][1])-m2)<1.:
                        if int(k) in xsecDict:
                            xsec = xsecDict[int(k)]
                            line = line.replace('-999007. -999007.',str(xsec)+' '+str(xsec*ul))
            elif col_type=='Channel':
                chan = int(float(line.split()[-1]))
                ul = float(line.split()[-10])
                if chan in xsecDict:
                    xsec = xsecDict[chan]
                    line = line.replace('-999007. -999007.',str(xsec)+' '+str(xsec*ul))
                    if str(chan) in massesDict:
                        split_line=line.split()
                        split_line[-1]="%s %s"%(massesDict[str(chan)][0],massesDict[str(chan)][1])
                        line = " ".join(split_line)
                    else: print 'ERROR: unable to find channel ',str(chan)
                
        fout.write(line+'\n')
    fout.close()
    print harvestfile
    
late2012AddXSec()
