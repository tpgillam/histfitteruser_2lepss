#include "late2012AllGridsPlottingMacro.C"

void late2012MakeContourPlots(
			      const char* inputFileNom = "Mc12SusyGtt_nom_late2012SameSign_output_hypotest__1_harvest_list.root", 
			      const char* inputFileUp = "Mc12SusyGtt_up_late2012SameSign_output_hypotest__1_harvest_list.root", 
			      const char* inputFileDown = "Mc12SusyGtt_down_late2012SameSign_output_hypotest__1_harvest_list.root", 
			      const char* upperlimitListFile = "None", 
			      const char* signalRegion0b = "", 
			      const char* signalRegion1b = "", 
			      const char* signalRegion3b = "", 			    
			      const char* xLabel = "notSpecified",
			      const char* yLabel = "notSpecified",
			      const char* processDescription = "unknown grid",
			      const char* forbiddenRegionCut = "0",
			      const char* forbiddenLabelX = "0",
			      const char* forbiddenLabelY = "0",
			      const char* forbiddenLabelText = "" )
{

  bool showsignal = true;
  int  discexcl; // 0=discovery, 1=exclusion
  bool showtevatron(false);
  bool doOneSigmaBand(false);

  double forbiddenRegionCutDouble = atof(forbiddenRegionCut);
  double forbiddenLabelXDouble = atof(forbiddenLabelX);
  double forbiddenLabelYDouble = atof(forbiddenLabelY);

  // Display excluded cross-section for Gtt, sbottom (lsp60) and gluino/squark via sleptons
  
//   TString str=inputFileNom;
//   if(str.Contains("SusyGtt_")) showsignal=true;
//   if(str.Contains("SusySbottom_TopChargino_N60_")) showsignal=true;
//   if(str.Contains("SusyGG2step_slep_")) showsignal=true;
//   if(str.Contains("SusySMSS2CNsl_")) showsignal=true;

  late2012MakeExclusion(inputFileNom, inputFileUp, inputFileDown, "SR", 5.8, showsignal, 1, upperlimitListFile, showtevatron, signalRegion0b, signalRegion1b, signalRegion3b, xLabel, yLabel, processDescription, forbiddenRegionCutDouble, forbiddenLabelXDouble, forbiddenLabelYDouble, forbiddenLabelText, true);
  late2012MakeExclusion(inputFileNom, inputFileUp, inputFileDown, "SR", 5.8, showsignal, 1, upperlimitListFile, showtevatron, signalRegion0b, signalRegion1b, signalRegion3b, xLabel, yLabel, processDescription, forbiddenRegionCutDouble, forbiddenLabelXDouble, forbiddenLabelYDouble, forbiddenLabelText, false);
  
}


/*   +++++ Example of arguments +++++++

void late2012MakeContourPlots(
			      const char* inputFileNom = "Mc12SusyGtt_nom_CONF_late2012SameSign_output_hypotest__1_harvest_list.root", 
			      const char* inputFileUp = "Mc12SusyGtt_up_CONF_late2012SameSign_output_hypotest__1_harvest_list.root", 
			      const char* inputFileDown = "Mc12SusyGtt_down_CONF_late2012SameSign_output_hypotest__1_harvest_list.root", 
			      const char* inputFileList = "Mc12SusyGtt_nom_CONF_late2012SameSign_output_upperlimit__1_harvest_list", 
			      const char* signalRegion0b = "None", 
			      const char* signalRegion1b = "None", 
			      const char* signalRegion3b = "None", 			    
			      const char* xLabel = "m_{#tilde{g}} [GeV]",
			      const char* yLabel = "m_{#tilde{#chi}^{0}_{1}} [GeV]",
			      const char* processDescription = "#tilde{g}-#tilde{g} production, #tilde{g}#rightarrow t#bar{t}#tilde{#chi}^{0}_{1}, m(#tilde{t}) >> m(#tilde{g})",
			      const char* forbiddenRegionCut = "350",
			      const char* forbiddenLabelX = "550",
			      const char* forbiddenLabelY = "230",
			      const char* forbiddenLabelText = "#tilde{g}#rightarrow t#bar{t}#tilde{#chi}^{0}_{1} forbidden" )
*/


