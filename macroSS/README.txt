

#folder: python
ModelIndependent.py -> used to obtain model independent results(nsig == 1) 
  : run just -w to create the worspace
		-> UpperLimitTable_SR4jTEl.py (macros/contourplot) -> here as input is the workspace created before
		-> you need also UpperLimitTex.py file
		
#folder: python		
TwoSSLep.py -> used to obtain exclusion limits
**mSUGRA model
  : run -w -p to create the workspace and the hypotest results
  : cd mocros/cpntourplot
  : root -l makelistfiles.C
  : root -l makecontourhists.C
  : root -l makecontourplots.C (input 3 root file: Nominal, Up, Down)
  
**gTT model
  : run -w -l to obtain the upper limit result
  : python addXSecToListFiles_Ev.py -> input file: Upper limit result
  
  : run -w -p to create the workspace and the hypotest results
  : root -l makelistfiles_SM.C
  : root -l makesmoothcontourhists.C
  : root -l makecontourplots_example_showExcludedXsection.C (input 4 root files: Nominal, Up, Down, Upper Limit)
    -> model dependent: showsignal true --> gives excluded model xsection
 