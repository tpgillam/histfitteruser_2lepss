#!/usr/bin/env python

def main():
    import ROOT, sys
    command = sys.argv[1]
    splitKeywords = ['PROCESS', 'YAXIS', 'FORBIDDENLABEL']

    specialCommandsDict = {}
    for splitKeyword in splitKeywords:
        if len(command.split(splitKeyword)) > 1:
            specialCommandsDict[splitKeyword] = command.split(splitKeyword)[1]
    
    command = command.replace(', ', '\", \"')
    command = command.replace('(', '(\"')
    command = command.replace(')', '\")')

    for splitKeyword in splitKeywords:
        if len(command.split(splitKeyword)) >1:
            command = command.split(splitKeyword)[0]+specialCommandsDict[splitKeyword]+command.split(splitKeyword)[2]

    
    print command
    ROOT.gROOT.ProcessLine(command)

def makeCommandRootFriendly(command, splitKeywords):
    # an ugly hack to get the handing over from python to root to work.
    # Apologies to future Thibaut as well as anyone else who has to look at this
    splitCommands = command.split(keyword)
    if len(splitCommands) > 1:
        elements = [0, 2]
    else:
        elements = [0]
    for i in elements:
        splitCommands[i] = splitCommands[i].replace(', ', '\", \"')
        splitCommands[i] = splitCommands[i].replace('(', '(\"')
        splitCommands[i] = splitCommands[i].replace(')', '\")')
    command = ''
    for partialCommand in splitCommands:
            command += partialCommand
    return command

main()
