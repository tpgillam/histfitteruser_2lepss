#!/usr/bin/env python
# vim: tabstop=8 shiftwidth=4 softtabstop=4 expandtab :
import os,sys

def retrieveBasePath():
    pwd = os.getcwd()
    pos = pwd.find('MET_jets_2lep_SS/')
    if pos < 0:
        raise NameError('Unable to find HistFitterUser path')
    basepath = pwd[:pos+len('MET_jets_2lep_SS/')]
    print 'Setting base path to %s'%(basepath)
    return basepath 

basePath = retrieveBasePath()
sys.path.append(basePath+'/python')
import pathUtilities, signalSameSignTools


def late2012AddXSec():
    if len(sys.argv)<2:
        print 'ERROR: should supply harvest file name'
        return

    #col_type='Channel'
    col_type='Masses'
    
    harvestfile = sys.argv[1]
    
    gridName = ''
    for string in harvestfile.split('_'):
        if string != 'nom':
            gridName += string+'_'
        else:
            gridName = gridName.strip('_')
            break
    massesDict = signalSameSignTools.getRunnumberMassesDictSS(gridName)
    print gridName
    
    xsecDict = {}
    fxsec = open(basePath+'share/crossSectionInfo','r')
    buffer = fxsec.read().splitlines()
    for line in buffer:
        line=line.strip()
        if len(line)>10 and not line.startswith('#'):
            tokens = line.split()
            xsecDict[int(tokens[0])]=float(tokens[1])

    fin = open(harvestfile,'r')
    buffer = fin.read().splitlines()
    fin.close()
    fout = open(harvestfile,'w')
    for line in buffer:
        if len(line)>10:
            if col_type=='Masses':
                m1 = float(line.split()[-2])
                m2 = float(line.split()[-1])
                ul = float(line.split()[-17])
                dsid = int(float(line.split()[-11]))
                if dsid in xsecDict:
                    filterEfficiency = getFilterEfficiency(str(dsid))
                    xsec = xsecDict[dsid]*filterEfficiency
                    line = line.replace('-999007. -999007.',str(xsec)+' '+str(xsec*ul))
                else:
                    print m1,m2,ul,dsid
                    print "ERROR: couldn't find xsec for dataset ID {}".format(dsid)
            elif col_type=='Channel':
                chan = int(float(line.split()[-6]))
                ul = float(line.split()[-10])
                if chan in xsecDict:
                    xsec = xsecDict[chan]
                    line = line.replace('-999007. -999007.',str(xsec)+' '+str(xsec*ul))
                    if str(chan) in massesDict:
                        split_line=line.split()
                        split_line[-1]="%s %s"%(massesDict[str(chan)][0],massesDict[str(chan)][1])
                        line = " ".join(split_line)
                    else: print 'ERROR: unable to find channel ',str(chan)
                
        fout.write(line+'\n')
    fout.close()
    print harvestfile
    


def getFilterEfficiency(runNumber):
    if isinstance(runNumber, str):
        runNumber = int(runNumber)
        if runNumber == 172159: return 3.2147E-01
        elif runNumber == 172160: return 3.3821E-01
        elif runNumber == 172161: return 3.4516E-01
        elif runNumber == 172162: return 3.5946E-01
        elif runNumber == 172163: return 3.7590E-01
        elif runNumber == 172164: return 3.6691E-01
        elif runNumber == 172165: return 3.6619E-01
        elif runNumber == 172166: return 3.7817E-01
        elif runNumber == 172167: return 3.9205E-01
        elif runNumber == 172168: return 3.8807E-01
        elif runNumber == 172169: return 3.8250E-01
        elif runNumber == 172170: return 3.8458E-01
        elif runNumber == 172171: return 3.8228E-01
        elif runNumber == 172172: return 3.9364E-01
        elif runNumber == 172173: return 4.0258E-01
        elif runNumber == 172174: return 3.9423E-01
        elif runNumber == 172175: return 4.0033E-01
        elif runNumber == 172176: return 3.9924E-01
        elif runNumber == 172177: return 3.9240E-01
        elif runNumber == 172178: return 3.8218E-01
        elif runNumber == 172179: return 3.9462E-01
        elif runNumber == 172180: return 4.0652E-01
        elif runNumber == 172181: return 4.2276E-01
        elif runNumber == 172182: return 4.1375E-01
        elif runNumber == 172183: return 4.1308E-01
        elif runNumber == 172184: return 4.0715E-01
        elif runNumber == 172185: return 4.0301E-01
        elif runNumber == 172186: return 3.9701E-01
        elif runNumber == 172187: return 3.9230E-01
        elif runNumber == 172188: return 3.8870E-01
        elif runNumber == 172189: return 4.0975E-01
        elif runNumber == 172190: return 4.2105E-01
        elif runNumber == 172191: return 4.3188E-01
        elif runNumber == 172192: return 4.2628E-01
        elif runNumber == 172193: return 4.2523E-01
        elif runNumber == 172194: return 4.2238E-01
        elif runNumber == 172195: return 4.1520E-01
        elif runNumber == 172196: return 4.1309E-01
        elif runNumber == 172197: return 4.0614E-01
        elif runNumber == 172198: return 4.0597E-01
        elif runNumber == 172199: return 3.9894E-01
        elif runNumber == 172200: return 3.9022E-01
        elif runNumber == 172201: return 4.2226E-01
        elif runNumber == 172202: return 4.2944E-01
        elif runNumber == 172203: return 4.4404E-01
        elif runNumber == 172204: return 4.4026E-01
        elif runNumber == 172205: return 4.3972E-01
        elif runNumber == 172206: return 4.3576E-01
        elif runNumber == 172207: return 4.3423E-01
        elif runNumber == 172208: return 4.2658E-01
        elif runNumber == 172209: return 4.2547E-01
        elif runNumber == 172210: return 4.2057E-01
        elif runNumber == 172211: return 4.1317E-01
        elif runNumber == 172212: return 4.0705E-01
        elif runNumber == 172213: return 4.0111E-01
        elif runNumber == 172214: return 3.9588E-01
        elif runNumber == 172215: return 4.3112E-01
        elif runNumber == 172216: return 4.4365E-01
        elif runNumber == 172217: return 4.5032E-01
        elif runNumber == 172218: return 4.4773E-01
        elif runNumber == 172219: return 4.4807E-01
        elif runNumber == 172220: return 4.4377E-01
        elif runNumber == 172221: return 4.4404E-01
        elif runNumber == 172222: return 4.3605E-01
        elif runNumber == 172223: return 4.4033E-01
        elif runNumber == 172224: return 4.2884E-01
        elif runNumber == 172225: return 4.3441E-01
        elif runNumber == 172226: return 4.2299E-01
        elif runNumber == 172227: return 4.1898E-01
        elif runNumber == 172228: return 4.1295E-01
        elif runNumber == 172229: return 4.0624E-01
        elif runNumber == 172230: return 3.9401E-01
        elif runNumber == 172231: return 4.4050E-01
        elif runNumber == 172232: return 4.4611E-01
        elif runNumber == 172233: return 4.5994E-01
        elif runNumber == 172234: return 4.5605E-01
        elif runNumber == 172235: return 4.6226E-01
        elif runNumber == 172236: return 4.5356E-01
        elif runNumber == 172237: return 4.5703E-01
        elif runNumber == 172238: return 4.4997E-01
        elif runNumber == 172239: return 4.4844E-01
        elif runNumber == 172240: return 4.4602E-01
        elif runNumber == 172241: return 4.4420E-01
        elif runNumber == 172242: return 4.3330E-01
        elif runNumber == 172243: return 4.3848E-01
        elif runNumber == 172244: return 4.2682E-01
        elif runNumber == 172245: return 4.1968E-01
        elif runNumber == 172246: return 4.1255E-01
        elif runNumber == 172247: return 4.0887E-01
        elif runNumber == 172248: return 3.9880E-01
        elif runNumber == 172249: return 4.5891E-01
        elif runNumber == 172250: return 4.6278E-01
        elif runNumber == 172251: return 4.7020E-01
        elif runNumber == 172252: return 4.6627E-01
        elif runNumber == 172253: return 4.6375E-01
        elif runNumber == 172254: return 4.5824E-01
        elif runNumber == 172255: return 4.6234E-01
        elif runNumber == 172256: return 4.5909E-01
        elif runNumber == 172257: return 4.5825E-01
        elif runNumber == 172258: return 4.5667E-01
        elif runNumber == 172259: return 4.5298E-01
        elif runNumber == 172260: return 4.4787E-01
        elif runNumber == 172261: return 4.4128E-01
        elif runNumber == 172262: return 4.4145E-01
        elif runNumber == 172263: return 4.3424E-01
        elif runNumber == 172264: return 4.2883E-01
        elif runNumber == 172265: return 4.2431E-01
        elif runNumber == 172266: return 4.1646E-01
        elif runNumber == 172267: return 4.0746E-01
        elif runNumber == 172268: return 4.0079E-01
        elif runNumber == 172269: return 4.6520E-01
        elif runNumber == 172270: return 4.6768E-01
        elif runNumber == 172271: return 4.7905E-01
        elif runNumber == 172272: return 4.7020E-01
        elif runNumber == 172273: return 4.7656E-01
        elif runNumber == 172274: return 4.7095E-01
        elif runNumber == 172275: return 4.7277E-01
        elif runNumber == 172276: return 4.6688E-01
        elif runNumber == 172277: return 4.6677E-01
        elif runNumber == 172278: return 4.6305E-01
        elif runNumber == 172279: return 4.6336E-01
        elif runNumber == 172280: return 4.5615E-01
        elif runNumber == 172281: return 4.5436E-01
        elif runNumber == 172282: return 4.5554E-01
        elif runNumber == 172283: return 4.4427E-01
        elif runNumber == 172284: return 4.4540E-01
        elif runNumber == 172285: return 4.4183E-01
        elif runNumber == 172286: return 4.3289E-01
        elif runNumber == 172287: return 4.2748E-01
        elif runNumber == 172288: return 4.1985E-01
        elif runNumber == 172289: return 4.0838E-01
        elif runNumber == 172290: return 4.0397E-01
        elif runNumber == 172291: return 4.6863E-01
        elif runNumber == 172292: return 4.7842E-01
        elif runNumber == 172293: return 4.8592E-01
        elif runNumber == 172294: return 4.7965E-01
        elif runNumber == 172295: return 4.8386E-01
        elif runNumber == 172296: return 4.7664E-01
        elif runNumber == 172297: return 4.7822E-01
        elif runNumber == 172298: return 4.7666E-01
        elif runNumber == 172299: return 4.7577E-01
        elif runNumber == 172300: return 4.7394E-01
        elif runNumber == 172301: return 4.6992E-01
        elif runNumber == 172302: return 4.6372E-01
        elif runNumber == 172303: return 4.6580E-01
        elif runNumber == 172304: return 4.6231E-01
        elif runNumber == 172305: return 4.6445E-01
        elif runNumber == 172306: return 4.5094E-01
        elif runNumber == 172307: return 4.5510E-01
        elif runNumber == 172308: return 4.4483E-01
        elif runNumber == 172309: return 4.4588E-01
        elif runNumber == 172310: return 4.3088E-01
        elif runNumber == 172311: return 4.3044E-01
        elif runNumber == 172312: return 4.2448E-01
        elif runNumber == 172313: return 4.1555E-01
        elif runNumber == 172314: return 4.0856E-01
        elif runNumber == 172315: return 4.7978E-01
        elif runNumber == 172316: return 4.8412E-01
        elif runNumber == 172317: return 4.9156E-01
        elif runNumber == 172318: return 4.8748E-01
        elif runNumber == 172319: return 4.8639E-01
        elif runNumber == 172320: return 4.8470E-01
        elif runNumber == 172321: return 4.8695E-01
        elif runNumber == 172322: return 4.8518E-01
        elif runNumber == 172323: return 4.8222E-01
        elif runNumber == 172324: return 4.7871E-01
        elif runNumber == 172325: return 4.7901E-01
        elif runNumber == 172326: return 4.7430E-01
        elif runNumber == 172327: return 4.7661E-01
        elif runNumber == 172328: return 4.7327E-01
        elif runNumber == 172329: return 4.7402E-01
        elif runNumber == 172330: return 4.6034E-01
        elif runNumber == 172331: return 4.6450E-01
        elif runNumber == 172332: return 4.5662E-01
        elif runNumber == 172333: return 4.5197E-01
        elif runNumber == 172334: return 4.4976E-01
        elif runNumber == 172335: return 4.4507E-01
        elif runNumber == 172336: return 4.3514E-01
        elif runNumber == 172337: return 4.3315E-01
        elif runNumber == 172338: return 4.2569E-01
        elif runNumber == 172339: return 4.1644E-01
        elif runNumber == 172340: return 4.0760E-01
        elif runNumber == 172341: return 4.8161E-01
        elif runNumber == 172342: return 4.9023E-01
        elif runNumber == 172343: return 4.9522E-01
        elif runNumber == 172344: return 4.9624E-01
        elif runNumber == 172345: return 4.9525E-01
        elif runNumber == 172346: return 4.9397E-01
        elif runNumber == 172347: return 5.0061E-01
        elif runNumber == 172348: return 4.8721E-01
        elif runNumber == 172349: return 4.8815E-01
        elif runNumber == 172350: return 4.8438E-01
        elif runNumber == 172351: return 4.8796E-01
        elif runNumber == 172352: return 4.8616E-01
        elif runNumber == 172353: return 4.8823E-01
        elif runNumber == 172354: return 4.8028E-01
        elif runNumber == 172355: return 4.8005E-01
        elif runNumber == 172356: return 4.7057E-01
        elif runNumber == 172357: return 4.7389E-01
        elif runNumber == 172358: return 4.6652E-01
        elif runNumber == 172359: return 4.6722E-01
        elif runNumber == 172360: return 4.6176E-01
        elif runNumber == 172361: return 4.6021E-01
        elif runNumber == 172362: return 4.5441E-01
        elif runNumber == 172363: return 4.4410E-01
        elif runNumber == 172364: return 4.3972E-01
        elif runNumber == 172365: return 4.3265E-01
        elif runNumber == 172366: return 4.2408E-01
        elif runNumber == 172367: return 4.1898E-01
        elif runNumber == 172368: return 4.1006E-01
        elif runNumber == 172369: return 4.9284E-01
        elif runNumber == 172370: return 4.9615E-01
        elif runNumber == 172371: return 5.0391E-01
        elif runNumber == 172372: return 5.0054E-01
        elif runNumber == 172373: return 5.0178E-01
        elif runNumber == 172374: return 4.9809E-01
        elif runNumber == 172375: return 4.9939E-01
        elif runNumber == 172376: return 4.9433E-01
        elif runNumber == 172377: return 4.9351E-01
        elif runNumber == 172378: return 4.8970E-01
        elif runNumber == 172379: return 4.9231E-01
        elif runNumber == 172380: return 4.8820E-01
        elif runNumber == 172381: return 4.9425E-01
        elif runNumber == 172382: return 4.8405E-01
        elif runNumber == 172383: return 4.8726E-01
        elif runNumber == 172384: return 4.8099E-01
        elif runNumber == 172385: return 4.8195E-01
        elif runNumber == 172386: return 4.8271E-01
        elif runNumber == 172387: return 4.7306E-01
        elif runNumber == 172388: return 4.7223E-01
        elif runNumber == 172389: return 4.6608E-01
        elif runNumber == 172390: return 4.6106E-01
        elif runNumber == 172391: return 4.6164E-01
        elif runNumber == 172392: return 4.5558E-01
        elif runNumber == 172393: return 4.4850E-01
        elif runNumber == 172394: return 4.4469E-01
        elif runNumber == 172395: return 4.3594E-01
        elif runNumber == 172396: return 4.3211E-01
        elif runNumber == 172397: return 4.1760E-01
        elif runNumber == 172398: return 4.0976E-01
        elif runNumber == 175911: return 0.37485
        elif runNumber == 175912: return 0.37911
        elif runNumber == 175913: return 0.37779
        elif runNumber == 175914: return 0.38527
        elif runNumber == 175915: return 0.38048
        elif runNumber == 175916: return 0.3867
        elif runNumber == 175917: return 0.38159
        elif runNumber == 175918: return 0.39412
        elif runNumber == 175919: return 0.38396
        elif runNumber == 175920: return 0.39392
        elif runNumber == 175921: return 0.3919
        elif runNumber == 175922: return 0.39165
        elif runNumber == 175923: return 0.38898
        elif runNumber == 175924: return 0.39751
        elif runNumber == 173295: return 0.31436
        elif runNumber == 173296: return 0.32284
        elif runNumber == 173297: return 0.31384
        elif runNumber == 173298: return 0.33303
        elif runNumber == 173299: return 0.32531
        elif runNumber == 173300: return 0.3138
        elif runNumber == 173301: return 0.34011
        elif runNumber == 173302: return 0.32943
        elif runNumber == 173303: return 0.32499
        elif runNumber == 173304: return 0.31346
        elif runNumber == 173305: return 0.34971
        elif runNumber == 173306: return 0.33927
        elif runNumber == 173307: return 0.33161
        elif runNumber == 173308: return 0.32583
        elif runNumber == 173309: return 0.31102
        elif runNumber == 173310: return 0.35925
        elif runNumber == 173311: return 0.34616
        elif runNumber == 173312: return 0.32958
        elif runNumber == 173313: return 0.31736
        elif runNumber == 173314: return 0.37668
        elif runNumber == 173315: return 0.36164
        elif runNumber == 173316: return 0.34144
        elif runNumber == 173317: return 0.33187
        elif runNumber == 173318: return 0.31943
        elif runNumber == 173319: return 0.39482
        elif runNumber == 173320: return 0.3747
        elif runNumber == 173321: return 0.35393
        elif runNumber == 173322: return 0.34303
        elif runNumber == 173323: return 0.33054
        elif runNumber == 173324: return 0.31472
        elif runNumber == 173325: return 0.40228
        elif runNumber == 173326: return 0.37936
        elif runNumber == 173327: return 0.36041
        elif runNumber == 173328: return 0.35623
        elif runNumber == 173329: return 0.34703
        elif runNumber == 173330: return 0.32975
        elif runNumber == 173331: return 0.31378
        elif runNumber == 173332: return 0.40552
        elif runNumber == 173333: return 0.38781
        elif runNumber == 173334: return 0.37696
        elif runNumber == 173335: return 0.36633
        elif runNumber == 173336: return 0.35456
        elif runNumber == 173337: return 0.34383
        elif runNumber == 173338: return 0.33598
        elif runNumber == 173339: return 0.3152
        elif runNumber == 173340: return 0.42166
        elif runNumber == 173341: return 0.40532
        elif runNumber == 173342: return 0.38923
        elif runNumber == 173343: return 0.37916
        elif runNumber == 173344: return 0.36406
        elif runNumber == 173345: return 0.34296
        elif runNumber == 173346: return 0.33616
        elif runNumber == 173347: return 0.32772
        elif runNumber == 173348: return 0.31287
        elif runNumber == 173349: return 0.43486
        elif runNumber == 173350: return 0.40614
        elif runNumber == 173351: return 0.40032
        elif runNumber == 173352: return 0.38568
        elif runNumber == 173353: return 0.36955
        elif runNumber == 173354: return 0.36512
        elif runNumber == 173355: return 0.35773
        elif runNumber == 173356: return 0.33641
        elif runNumber == 173357: return 0.33254
        elif runNumber == 173358: return 0.31215
        elif runNumber == 173359: return 0.43133
        elif runNumber == 173360: return 0.41806
        elif runNumber == 173361: return 0.40254
        elif runNumber == 173362: return 0.38405
        elif runNumber == 173363: return 0.39035
        elif runNumber == 173364: return 0.37676
        elif runNumber == 173365: return 0.3594
        elif runNumber == 173366: return 0.34381
        elif runNumber == 173367: return 0.34669
        elif runNumber == 173368: return 0.33174
        elif runNumber == 173369: return 0.31299
        elif runNumber == 173370: return 0.44627
        elif runNumber == 173371: return 0.42474
        elif runNumber == 173372: return 0.41463
        elif runNumber == 173373: return 0.39648
        elif runNumber == 173374: return 0.38865
        elif runNumber == 173375: return 0.38127
        elif runNumber == 173376: return 0.36922
        elif runNumber == 173377: return 0.3643
        elif runNumber == 173378: return 0.35333
        elif runNumber == 173379: return 0.34127
        elif runNumber == 173380: return 0.33205
        elif runNumber == 173381: return 0.30805
        elif runNumber == 173382: return 0.45179
        elif runNumber == 173383: return 0.43452
        elif runNumber == 173384: return 0.41915
        elif runNumber == 173385: return 0.40216
        elif runNumber == 173386: return 0.40174
        elif runNumber == 173387: return 0.37982
        elif runNumber == 173388: return 0.37106
        elif runNumber == 173389: return 0.36256
        elif runNumber == 173390: return 0.36005
        elif runNumber == 173391: return 0.35147
        elif runNumber == 173392: return 0.34334
        elif runNumber == 173393: return 0.32804
        elif runNumber == 173394: return 0.32076
        return 1.0

if __name__ == '__main__':
    late2012AddXSec()
