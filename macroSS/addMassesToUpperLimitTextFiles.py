#!/usr/bin/env python
# vim: tabstop=8 shiftwidth=4 softtabstop=4 expandtab :

def main():
    import sys, os
    sys.path.append('../python')
    import pathUtilities
    outName = sys.argv[1]

    import signalSameSignTools
    signalRunnumberToMassesDict = signalSameSignTools.getRunnumbersMassesDictSSAllGrids()
    
    for fileName in os.listdir(os.path.join(pathUtilities.histFitterTopDirectory(), 'macroSS')):
        if outName in fileName and 'upperlimit' in fileName and not fileName.endswith('_updated') :
            grid = getGridForFileName(fileName, signalRunnumberToMassesDict)
            openFile = open(fileName, "r")
            outFile = open(fileName+'_updated', "w+")
            for line in openFile:
                runNumber = int(float(line.split()[-9]))
                try:
                    masses = signalRunnumberToMassesDict [grid][str(runNumber)]
                    newLine = line.strip() + "  " +  str(masses[0]) + '   '+ str(masses[1]) + '\n'
                    outFile.write(newLine)
                except: print "PROBLEM! This runnumber is not in the masses dict: ", grid, runNumber
            openFile.close()
            outFile.close()
                    

def  getGridForFileName(fileName, signalRunnumberToMassesDict):
    if fileName.startswith('Mc12SusyGluinoStopCharm'): return 'Mc12SusyGluinoStopCharm'
    else:
        for grid in signalRunnumberToMassesDict:
            if grid in fileName: 
                return grid


if __name__ == '__main__':
    main()
