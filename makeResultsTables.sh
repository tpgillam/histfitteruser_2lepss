#!/bin/sh

TheHistDir=/r02/atlas/thibaut/histFitterSummer2013/HistFitterUser/MET_jets_2lep_SS
TheINTDir=/r02/atlas/thibaut/histFitterSummer2013/tables

###########

HistFitter.py -twf -D before,after -u "--additionalOutName VRVVttV --signalList bkgOnly --validationRegions VRVV,VRttW,VRttZ --signalRegions SRall_disc --fitMode discovery" python/summer2013SameSign.py

YieldsTable.py -b -c VRVV_meff,VRttW_meff,VRttZ_meff -s '[topZ,topW,ttbarHiggs,singleTopZ,fourTop],[diBosonWZ,diBosonSS,diBosonPowhegZZ,triBoson],fakes,chargeFlip' -w results/Discovery_VRVVttV_summer2013SameSign/bkgOnly_combined_NormalMeasurement_model_afterFit.root -o MyYieldsTableVR.tex -u "in the VRVV, VRttW and VRttZ channels" -t table.results.yields.VRs

cp $TheHistDir/MyYieldsTableVR.tex  $TheINTDir/.

################### Fine Tables ##############

HistFitter.py -twf -D before,after -u "--additionalOutName VRVVttVFineBackgrounds --signalList bkgOnly --validationRegions VRVV,VRttW,VRttZ --signalRegions SRall_disc --fitMode discovery " python/summer2013SameSign.py

YieldsTable.py -b -c VRVV_meff,VRttW_meff,VRttZ_meff -s  topW,singleTopZ,topZ,fourTop,diBosonWZ,diBosonSS,triBoson,diBosonPowhegZZ,ttbarHiggs,fakes,chargeFlip -w results/Discovery_VRVVttVFineBackgrounds_summer2013SameSign/bkgOnly_combined_NormalMeasurement_model_afterFit.root -o MyYieldsTableVRFine.tex -u "in the VRVV, VRttW and VRttZ channels" -t table.results.yields.VRs.Fine

cp $TheHistDir/MyYieldsTableVRFine.tex  $TheINTDir/.

#######################


HistFitter.py -twf -u "--signalList bkgOnly --additionalOutName SRallDiscoveryFine --signalRegions SRall_disc --validationRegions SR0b_disc,SR1b_disc,SR3b_disc,SR3Llow_disc,SR3Lhigh_disc --fitMode discovery --useStat PerSample" python/summer2013SameSign.py

YieldsTable.py -b -c SR0b_disc_cuts,SR1b_disc_cuts,SR3b_disc_cuts,SR3Llow_disc_cuts,SR3Lhigh_disc_cuts -s topW,singleTopZ,topZ,diBosonWZ,diBosonSS,triBoson,fourTop,diBosonPowhegZZ,ttbarHiggs,fakes,chargeFlip -w results/Discovery_SRallDiscoveryFine_summer2013SameSign/bkgOnly_combined_NormalMeasurement_model_afterFit.root -o MyYieldsTableSRFine.tex -u "in signal regions SR0b, SR1b, SR3b, SR3Llow and SR3Lhigh" -t table.results.yields.SRdisc.Fine

cp $TheHistDir/MyYieldsTableSRFine.tex  $TheINTDir/.



################

HistFitter.py -twf -D before,after -u "--additionalOutName VRttZchannels --signalList bkgOnly --validationRegions VRttZee,VRttZem,VRttZmm --signalRegions SRall_disc --fitMode discovery --useCoarseGranularity True" python/summer2013SameSign.py
YieldsTable.py -b -c VRttZee_meff,VRttZem_meff,VRttZmm_meff -s  '[topZ,topW,ttbarHiggs,singleTopZ,fourTop],[diBosonWZ,diBosonSS,diBosonPowhegZZ,triBoson],fakes,chargeFlip' -w results/Discovery_VRttZchannels_summer2013SameSign/bkgOnly_combined_NormalMeasurement_model_afterFit.root -o MyYieldsTableVRttZchannels.tex -u "in the VRttZee, VRttZem and VRttZem channels" -t table.results.yields.VRttZs

cp $TheHistDir/MyYieldsTableVRttZchannels.tex  $TheINTDir/.

#########################

HistFitter.py -twf -u "--signalList bkgOnly --additionalOutName SRallDiscovery --signalRegions SRall_disc --validationRegions SR0b_disc,SR1b_disc,SR3b_disc,SR3Llow_disc,SR3Lhigh_disc --fitMode discovery --useStat PerSample" python/summer2013SameSign.py

YieldsTable.py -b -c SR0b_disc_cuts,SR1b_disc_cuts,SR3b_disc_cuts,SR3Llow_disc_cuts,SR3Lhigh_disc_cuts -s '[topZ,topW,ttbarHiggs,singleTopZ,fourTop],[diBosonWZ,diBosonSS,diBosonPowhegZZ,triBoson],fakes,chargeFlip' -w results/Discovery_SRallDiscovery_summer2013SameSign/bkgOnly_combined_NormalMeasurement_model_afterFit.root -o MyYieldsTableSR.tex -u "in signal regions SR0b, SR1b, SR3b, SR3Llow and SR3Lhigh" -t table.results.yields.SRdisc

SysTable.py -c SR0b_disc_cuts -s '[topZ,topW,ttbarHiggs,singleTopZ,fourTop],[diBosonWZ,diBosonSS,diBosonPowhegZZ,triBoson],fakes,chargeFlip' -w results/Discovery_SRallDiscovery_summer2013SameSign/bkgOnly_combined_NormalMeasurement_model_afterFit.root -o MySystTableSR0b.tex python/MySystTableConfig.py
SysTable.py -c SR1b_disc_cuts -s '[topZ,topW,ttbarHiggs,singleTopZ,fourTop],[diBosonWZ,diBosonSS,diBosonPowhegZZ,triBoson],fakes,chargeFlip' -w results/Discovery_SRallDiscovery_summer2013SameSign/bkgOnly_combined_NormalMeasurement_model_afterFit.root -o MySystTableSR1b.tex python/MySystTableConfig.py
SysTable.py -c SR3b_disc_cuts -s '[topZ,topW,ttbarHiggs,singleTopZ,fourTop],[diBosonWZ,diBosonSS,diBosonPowhegZZ,triBoson],fakes,chargeFlip' -w results/Discovery_SRallDiscovery_summer2013SameSign/bkgOnly_combined_NormalMeasurement_model_afterFit.root -o MySystTableSR3b.tex python/MySystTableConfig.py
SysTable.py -c SR3Llow_disc_cuts -s '[topZ,topW,ttbarHiggs,singleTopZ,fourTop],[diBosonWZ,diBosonSS,diBosonPowhegZZ,triBoson],fakes' -w results/Discovery_SRallDiscovery_summer2013SameSign/bkgOnly_combined_NormalMeasurement_model_afterFit.root -o MySystTableSR3Llow.tex python/MySystTableConfig.py
SysTable.py -c SR3Lhigh_disc_cuts -s '[topZ,topW,ttbarHiggs,singleTopZ,fourTop],[diBosonWZ,diBosonSS,diBosonPowhegZZ,triBoson],fakes' -w results/Discovery_SRallDiscovery_summer2013SameSign/bkgOnly_combined_NormalMeasurement_model_afterFit.root -o MySystTableSR3Lhigh.tex python/MySystTableConfig.py

cp $TheHistDir/MyYieldsTableSR.tex  $TheINTDir/.
cp $TheHistDir/MySystTableSR0b.tex  $TheINTDir/.
cp $TheHistDir/MySystTableSR1b.tex  $TheINTDir/.
cp $TheHistDir/MySystTableSR3b.tex  $TheINTDir/.
cp $TheHistDir/MySystTableSR3Llow.tex  $TheINTDir/.
cp $TheHistDir/MySystTableSR3Lhigh.tex  $TheINTDir/.


HistFitter.py -twf -u "--signalList bkgOnly --additionalOutName SRallDiscovery --signalRegions SRall_disc --validationRegions SR0b_disc,SR1b_disc,SR3b_disc,SR3Llow_disc,SR3Lhigh_disc --fitMode discovery --useStat PerSample" python/summer2013SameSign.py


SysTable.py -c SR0b_disc_cuts -s '[topZ,topW,ttbarHiggs,singleTopZ,fourTop],[diBosonWZ,diBosonSS,diBosonPowhegZZ,triBoson],fakes,chargeFlip' -w results/Discovery_SRallDiscovery_summer2013SameSign/bkgOnly_combined_NormalMeasurement_model_afterFit.root -o MySystTableSR0bFine.tex
SysTable.py -c SR1b_disc_cuts -s '[topZ,topW,ttbarHiggs,singleTopZ,fourTop],[diBosonWZ,diBosonSS,diBosonPowhegZZ,triBoson],fakes,chargeFlip' -w results/Discovery_SRallDiscovery_summer2013SameSign/bkgOnly_combined_NormalMeasurement_model_afterFit.root -o MySystTableSR1bFine.tex
SysTable.py -c SR3b_disc_cuts -s '[topZ,topW,ttbarHiggs,singleTopZ,fourTop],[diBosonWZ,diBosonSS,diBosonPowhegZZ,triBoson],fakes,chargeFlip' -w results/Discovery_SRallDiscovery_summer2013SameSign/bkgOnly_combined_NormalMeasurement_model_afterFit.root -o MySystTableSR3bFine.tex
SysTable.py -c SR3Llow_disc_cuts -s '[topZ,topW,ttbarHiggs,singleTopZ,fourTop],[diBosonWZ,diBosonSS,diBosonPowhegZZ,triBoson],fakes' -w results/Discovery_SRallDiscovery_summer2013SameSign/bkgOnly_combined_NormalMeasurement_model_afterFit.root -o MySystTableSR3LlowFine.tex 
SysTable.py -c SR3Lhigh_disc_cuts -s '[topZ,topW,ttbarHiggs,singleTopZ,fourTop],[diBosonWZ,diBosonSS,diBosonPowhegZZ,triBoson],fakes' -w results/Discovery_SRallDiscovery_summer2013SameSign/bkgOnly_combined_NormalMeasurement_model_afterFit.root -o MySystTableSR3LhighFine.tex 

cp $TheHistDir/MySystTableSR0bFine.tex  $TheINTDir/.
cp $TheHistDir/MySystTableSR1bFine.tex  $TheINTDir/.
cp $TheHistDir/MySystTableSR3bFine.tex  $TheINTDir/.
cp $TheHistDir/MySystTableSR3LlowFine.tex  $TheINTDir/.
cp $TheHistDir/MySystTableSR3LhighFine.tex  $TheINTDir/.

#####

HistFitter.py -twf -u "--signalList bkgOnly --additionalOutName DiscEeEmMm --signalRegions SRall_disc --validationRegions SR0b_disc,SR0bee_disc,SR0bem_disc,SR0bmm_disc,SR1b_disc,SR1bee_disc,SR1bem_disc,SR1bmm_disc,SR3b_disc,SR3bee_disc,SR3bem_disc,SR3bmm_disc,SR3Llow_disc,SR3Llowee_disc,SR3Llowem_disc,SR3Llowmm_disc,SR3Lhigh_disc,SR3Lhighee_disc,SR3Lhighem_disc,SR3Lhighmm_disc --fitMode discovery --useStat PerSample" python/summer2013SameSign.py

YieldsTable.py -b -c SR0b_disc_cuts,SR0bee_disc_cuts,SR0bem_disc_cuts,SR0bmm_disc_cuts -s '[topZ,topW,ttbarHiggs,singleTopZ,fourTop],[diBosonWZ,diBosonSS,diBosonPowhegZZ,triBoson],fakes,chargeFlip' -w results/Discovery_DiscEeEmMm_summer2013SameSign/bkgOnly_combined_NormalMeasurement_model_afterFit.root -o MyYieldsTableSR0bPerFlavour.tex -u "in signal regions SR0b, split by lepton channels" -t table.results.yields.SR0bFlav
YieldsTable.py -b -c SR1b_disc_cuts,SR1bee_disc_cuts,SR1bem_disc_cuts,SR1bmm_disc_cuts -s '[topZ,topW,ttbarHiggs,singleTopZ,fourTop],[diBosonWZ,diBosonSS,diBosonPowhegZZ,triBoson],fakes,chargeFlip' -w results/Discovery_DiscEeEmMm_summer2013SameSign/bkgOnly_combined_NormalMeasurement_model_afterFit.root -o MyYieldsTableSR1bPerFlavour.tex -u "in signal regions SR1b, split by lepton channels" -t table.results.yields.SR1bFlav
YieldsTable.py -b -c SR3b_disc_cuts,SR3bee_disc_cuts,SR3bem_disc_cuts,SR3bmm_disc_cuts -s '[topZ,topW,ttbarHiggs,singleTopZ,fourTop],[diBosonWZ,diBosonSS,diBosonPowhegZZ,triBoson],fakes,chargeFlip' -w results/Discovery_DiscEeEmMm_summer2013SameSign/bkgOnly_combined_NormalMeasurement_model_afterFit.root -o MyYieldsTableSR3bPerFlavour.tex -u "in signal regions SR3b, split by lepton channels" -t table.results.yields.SR3bFlav
YieldsTable.py -b -c SR3Llow_disc_cuts,SR3Llowee_disc_cuts,SR3Llowem_disc_cuts,SR3Llowmm_disc_cuts -s  '[topZ,topW,ttbarHiggs,singleTopZ,fourTop],[diBosonWZ,diBosonSS,diBosonPowhegZZ,triBoson],fakes,chargeFlip' -w results/Discovery_DiscEeEmMm_summer2013SameSign/bkgOnly_combined_NormalMeasurement_model_afterFit.root -o MyYieldsTableSR3LlowPerFlavour.tex -u "in signal regions SR3Llow, split by lepton channels" -t table.results.yields.SR3LlowFlav
YieldsTable.py -b -c SR3Lhigh_disc_cuts,SR3Lhighee_disc_cuts,SR3Lhighem_disc_cuts,SR3Lhighmm_disc_cuts -s  '[topZ,topW,ttbarHiggs,singleTopZ,fourTop],[diBosonWZ,diBosonSS,diBosonPowhegZZ,triBoson],fakes,chargeFlip' -w results/Discovery_DiscEeEmMm_summer2013SameSign/bkgOnly_combined_NormalMeasurement_model_afterFit.root -o MyYieldsTableSR3LhighPerFlavour.tex -u "in signal regions SR3Lhigh, split by lepton channels" -t table.results.yields.SR3LhighFlav


cp $TheHistDir/MyYieldsTableSR0bPerFlavour.tex  $TheINTDir/.
cp $TheHistDir/MyYieldsTableSR1bPerFlavour.tex  $TheINTDir/.
cp $TheHistDir/MyYieldsTableSR3bPerFlavour.tex  $TheINTDir/.
cp $TheHistDir/MyYieldsTableSR3LlowPerFlavour.tex  $TheINTDir/.
cp $TheHistDir/MyYieldsTableSR3LhighPerFlavour.tex  $TheINTDir/.

##############

HistFitter.py -twf -u "--signalList bkgOnly --additionalOutName SRallDiscovery_BinByBin --signalRegions SRall_disc --validationRegions SR0b_bin1,SR0b_bin2,SR0b_bin3,SR0b_bin4,SR1b_bin1,SR1b_bin2,SR1b_bin3,SR3b_bin1,SR3b_bin2,SR3Llow_bin1,SR3Llow_bin2,SR3Lhigh_bin1,SR3Lhigh_bin2 --fitMode discovery" python/summer2013SameSign.py

YieldsTable.py -b -c SR0b_bin1_cuts,SR0b_bin2_cuts,SR0b_bin3_cuts,SR0b_bin4_cuts -s '[topZ,topW,ttbarHiggs,singleTopZ,fourTop],[diBosonWZ,diBosonSS,diBosonPowhegZZ,triBoson],fakes,chargeFlip' -w results/Discovery_SRallDiscovery_BinByBin_summer2013SameSign/bkgOnly_combined_NormalMeasurement_model_afterFit.root -o MyYieldsTableSR0b_ByBin.tex -u "in signal region SR0b, bin by bin" -t table.results.yields.SR0b.ByBin

YieldsTable.py -b -c SR1b_bin1_cuts,SR1b_bin2_cuts,SR1b_bin3_cuts,SR3b_bin1_cuts,SR3b_bin2_cuts -s '[topZ,topW,ttbarHiggs,singleTopZ,fourTop],[diBosonWZ,diBosonSS,diBosonPowhegZZ,triBoson],fakes,chargeFlip' -w results/Discovery_SRallDiscovery_BinByBin_summer2013SameSign/bkgOnly_combined_NormalMeasurement_model_afterFit.root -o MyYieldsTableSR1b3b_ByBin.tex -u "in signal region SR1b and SR3b, bin by bin" -t table.results.yields.SR1b3b.ByBin

YieldsTable.py -b -c SR3Llow_bin1_cuts,SR3Llow_bin2_cuts,SR3Lhigh_bin1_cuts,SR3Lhigh_bin2_cuts -s '[topZ,topW,ttbarHiggs,singleTopZ,fourTop],[diBosonWZ,diBosonSS,diBosonPowhegZZ,triBoson],fakes,chargeFlip' -w results/Discovery_SRallDiscovery_BinByBin_summer2013SameSign/bkgOnly_combined_NormalMeasurement_model_afterFit.root -o MyYieldsTableSR3L_ByBin.tex -u "in signal region SR3Llow and SR3Lhigh, bin by bin" -t table.results.yields.SR3L.ByBin

cp MyYieldsTableSR0b_ByBin.tex  $TheINTDir/.
cp MyYieldsTableSR1b3b_ByBin.tex  $TheINTDir/.
cp MyYieldsTableSR3L_ByBin.tex  $TheINTDir/.