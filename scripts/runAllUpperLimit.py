#!/usr/bin/env python
import os
baseDir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
os.chdir(baseDir)

def runDiscovery():
  command1 = '''HistFitter.py -twf -u "--signalList bkgOnly --additionalOutName SRallDiscovery --signalRegions SRall_disc --validationRegions SR0b_disc,SR1b_disc,SR3b_disc,SR3Llow_disc,SR3Lhigh_disc --fitMode discovery --useStat PerSample" python/summer2013SameSign.py'''
  command2 = '''YieldsTable.py -b -c SR0b_disc,SR1b_disc,SR3b_disc,SR3Llow_disc,SR3Lhigh_disc -s topZ,topW,diBoson,fakes,chargeFlip -w results/Discovery_SRallDiscovery_summer2013SameSign/bkgOnly_combined_NormalMeasurement_model_afterFit.root -o MyYieldsTableSR.tex -u "in signal regions SR0b, SR1b, SR3b, SR3Llow and SR3Lhigh" -t table.results.yields.SRdisc'''

  import subprocess
  subprocess.call(command1, shell=True)
  subprocess.call(command2, shell=True)

  SRs = ['SR0b', 'SR1b', 'SR3b', 'SR3L_low', 'SR3L_high']
  for index in range(len(SRs)):
    SRname = SRs[index]
    command3 = '''HistFitter.py -wf -u '--signalRegionIndex {0}' python/MyUserAnalysis.py'''.format(index)
    command4 = '''UpperLimitTable.py -c combined -p mu_Sig -w results/MyUserAnalysis/SPlusB_combined_NormalMeasurement_model.root -l 20.2814 -n 5000 -o UpperLimitTable_Sig_cuts_nToys5000_{0}_disc.tex'''.format(SRname)

    subprocess.call(command3, shell=True)
    subprocess.call(command4, shell=True)
  subprocess.call('rm MyYieldsTableSR.*', shell=True)



def runExclusion():
  command1 = '''HistFitter.py -twf -u "--signalList bkgOnly --additionalOutName SRallExclusion --signalRegions SRall --validationRegions SR0b,SR1b,SR3b,SR3Llow,SR3Lhigh --useStat PerSample" python/summer2013SameSign.py'''
  command2 = '''YieldsTable.py -b -c SR0b,SR1b,SR3b,SR3Llow,SR3Lhigh -s topZ,topW,diBoson,fakes,chargeFlip -w results/Mc12SusyGtt_SRallExclusion_summer2013SameSign/bkgOnly_combined_NormalMeasurement_model_afterFit.root -o MyYieldsTableSR.tex -u "in signal regions SR0b, SR1b, SR3b, SR3Llow and SR3Lhigh" -t table.results.yields.SRexcl'''

  import subprocess
  subprocess.call(command1, shell=True)
  subprocess.call(command2, shell=True)

  SRs = ['SR0b', 'SR1b', 'SR3b', 'SR3L_low', 'SR3L_high']
  for index in range(len(SRs)):
    SRname = SRs[index]
    command3 = '''HistFitter.py -wf -u '--signalRegionIndex {0}' python/MyUserAnalysis.py'''.format(index)
    command4 = '''UpperLimitTable.py -c combined -p mu_Sig -w results/MyUserAnalysis/SPlusB_combined_NormalMeasurement_model.root -l 20.2814 -n 5000 -o UpperLimitTable_Sig_cuts_nToys5000_{0}_excl.tex'''.format(SRname)

    subprocess.call(command3, shell=True)
    subprocess.call(command4, shell=True)
  subprocess.call('rm MyYieldsTableSR.*', shell=True)


if __name__ == '__main__':
  runDiscovery()
  runExclusion()
