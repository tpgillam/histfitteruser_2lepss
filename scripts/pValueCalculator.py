#!/usr/bin/env python

def main():
    import ROOT
    obsDict = getSRObservedDict()
    for sr in obsDict:
        print '\n'+sr, ROOT.RooStats.NumberCountingUtils.BinomialObsZ(obsDict[sr][0], obsDict[sr][1], obsDict[sr][2]/obsDict[sr][1])

def getSRObservedDict():
    return {
        'SR0b': (10, 4.59, 1.23),
        'SR1b': (9, 3.38, 1.64),
        'SR3b': (1, 1.44, 0.57),
        'SR3Llow': (5, 2.18, 1.36),
        'SR3Lhigh': (0, 1.95, 0.52),
        'SR0bee': (5, 0.97, 0.57),
        'SR1bee': (6, 1.24, 1.07),

        }



main()
