#!/bin/bash

cd ..

HistFitter.py -twf -u "--signalList bkgOnly --additionalOutName SRall --signalRegions SRall_disc --validationRegions SR0b,SR1b,SR3b,SR3LepOffShell,SR3LepOnShell,SR2b --fitMode discovery --useStat PerSample" python/summer2013SameSign.py

cd scripts

YieldsTable.py -b -c SR0b_disc_cuts,SR1b_disc_cuts,SR3b_disc_cuts -s topV,diBoson,fakes,chargeFlip -w ../results/Discovery_SRall_summer2013SameSign/bkgOnly_combined_NormalMeasurement_model_afterFit.root -o ../latexTablesOutput/yieldsTableSR.tex

SysTable.py -c SR0b_disc_cuts -s topV,diBoson,fakes,chargeFlip -w ../results/Discovery_SRall_summer2013SameSign/bkgOnly_combined_NormalMeasurement_model_afterFit.root -o ../latexTablesOutput/systTableSR0b.tex
 SysTable.py -c SR1b_disc_cuts -s topV,diBoson,fakes,chargeFlip -w ../results/Discovery_SRall_summer2013SameSign/bkgOnly_combined_NormalMeasurement_model_afterFit.root -o ../latexTablesOutput/systTableSR1b.tex
SysTable.py -c SR3b_disc_cuts -s topV,fakes,chargeFlip -w ../results/Discovery_SRall_summer2013SameSign/bkgOnly_combined_NormalMeasurement_model_afterFit.root -o ../latexTablesOutput/systTableSR3b.tex
SysTable.py -c SR3LepOnShell_disc_cuts -s topV,fakes,chargeFlip -w ../results/Discovery_SRall_summer2013SameSign/bkgOnly_combined_NormalMeasurement_model_afterFit.root -o ../latexTablesOutput/systTableSR3LepOnShell.tex
SysTable.py -c SR3LepOffShell_disc_cuts -s topV,fakes,chargeFlip -w ../results/Discovery_SRall_summer2013SameSign/bkgOnly_combined_NormalMeasurement_model_afterFit.root -o ../latexTablesOutput/systTableSR3LepOffShell.tex
SysTable.py -c SR2b_disc_cuts -s topV,fakes,chargeFlip -w ../results/Discovery_SRall_summer2013SameSign/bkgOnly_combined_NormalMeasurement_model_afterFit.root -o ../latexTablesOutput/systTableSR2b.texlatexTablesOutput/