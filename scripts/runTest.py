#!/usr/bin/env python
from ROOT import gROOT,gSystem,gDirectory
gSystem.Load("libSusyFitter.so")
from ROOT import Util
from cmdLineUtils import getPdfInRegions,getName

# This specific example was made like this:
# HistFitter.py -twf -u '--signalGridName Mc12SusyGG2step_slep --signalList signal173836' python/summer2013SameSign.py

#############################
# top-level user parameters #
#############################

wsFileName = 'results/Mc12SusyGtt_nom_1128ExperimentalUncertaintyRun180_summer2013SameSign/Mc12SusyGtt_signal156637_combined_NormalMeasurement_model_afterFit.root'
###'results/Mc12SusyRPVUDD_nom_temp1_summer2013SameSign/Mc12SusyRPVUDD_signal174320_combined_NormalMeasurement_model_afterFit.root'#"results/Mc12SusyGG2step_slep__summer2013SameSign/Mc12SusyGG2step_slep_signal173836_combined_NormalMeasurement_model_afterFit.root"

sample="signal156637"

regions=['SR0b_meff']

#everything should be automatic below this line...


###########################################################################
# Get names of relevant fit parameters (as in python/MySysTableconfig.py) #
###########################################################################
namemap={}
namemap['Experimental systematics']=[]
namemap['Data statistics']=[]
namemap['MC statistics']=[]

w = Util.GetWorkspaceFromFile(wsFileName,'w')
result = w.obj("RooExpandedFitResult_afterFit")
fpf = result.floatParsFinal() 
for idx in range(fpf.getSize()):
    parname = fpf[idx].GetName()
    if parname.startswith("gamma_shape_mcstat_fakes") or parname.startswith("gamma_shape_mcstat_misId_") or parname=="Lumi" or parname.startswith("alpha"):
        namemap['Experimental systematics'].append(parname)

    elif parname.startswith("gamma_stat_"): 
        namemap['MC statistics'].append(parname)

    elif parname=="mu_SIG":
        #namemap['Data statistics'].append(parname)
        pass
    else:
        print "WARNING: don't know what to do with nuisance parameter: %s. It will be ignored."%parname
        pass

print "Experimental systematics: %s \n"%namemap['Experimental systematics']
print "MC statistics: %s \n"%namemap['MC statistics']
print "Data statistics: %s \n"%namemap['Data statistics']



####################################################
# Calculate sum of uncertainties as in SysTable.py # 
####################################################
values={}

doAsym=True
result = w.obj("RooExpandedFitResult_afterFit")
for region in regions:
    pdfInRegion=getPdfInRegions(w,sample,region)
    values['yield_'+region]=pdfInRegion.getVal()

    # logic goes like this:
    # 1) set all parameters to constant except the parameters of interest
    # 2) calculate total uncertainty with GetPropagatedError [only picking floating parameters]
    fpf = result.floatParsFinal() 
    for idx in range(fpf.getSize()):
        #set all constant
        parname = fpf[idx].GetName()
        par = w.var(parname)
        par.setConstant()

    for key in namemap.keys():
        #re-set some parameters to non-constant
        for parname in namemap[key]:
            par = w.var(parname)
            par.setConstant(False)
            pass
        #calucate error in this configuration
        print  Util.GetPropagatedError(pdfInRegion, result,True)
        sysError  = Util.GetPropagatedError(pdfInRegion, result, doAsym)
        values[key+region] =  sysError
        #set back to constant
        for idx in range(fpf.getSize()):
            parname = fpf[idx].GetName()
            par = w.var(parname)
            par.setConstant()
            pass

print "values",values
